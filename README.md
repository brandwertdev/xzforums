# XZ Forums

Mobile Web App / Desktop Web App 

Forum für Game Community 

## Funktionsauszug
* User Anmeldung / Registration
* Rollen
* Up/Downvotes
* Kommentierfunktionen
* Profilverwaltung
* Private Nachrichten

## Technologien
* BackboneJS
* Underscore
* Jquery
* HTML
* Grunt
* Node
* GIT
* SASS
* i18next
* CasperJS