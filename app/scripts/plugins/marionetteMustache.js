define([
    'lodash',
    'marionette',
    'mustache'
], function (_, Marionette, Mustache) {
    'use strict';

    return {
        init: function () {
            _.extend(Marionette.TemplateCache.prototype, {
                loadTemplate: function(templateId){
                    var template = Marionette.$(templateId).html();

                    if (!template || template.length === 0){
                        throwError("Could not find template: '" + templateId + "'", "NoTemplateError");
                    }

                    return template;
                }
            });
        }
    };
});