define([
    'lodash',

    'marionette'
], function (_, Marionette) {
    _.extend(Marionette.View.prototype, {
        addController: function (controllers) {
            var self = this;

            if (!controllers || (_.isArray(controllers) && _.isEmpty(controllers)))
                return false;

            if (!_.isArray(controllers))
                controllers = [controllers];

            _.each(controllers, function(elem) {
                _.each(elem, function (val, key) {
                    if (self[key])
                        _.extend(self[key], val);
                    else if (_.isObject(val) && !_.isFunction(val))
                        self[key] = _.extend({}, val);
                    else
                        self[key] = val;
                });
            });
        },

        // Overriding Marionette's delegateEvents to handle
        // the `triggers`, `modelEvents`, and `collectionEvents`, 'permissionEvents' configuration
        delegateEvents: function(events){
            this._delegateDOMEvents(events);
            Marionette.bindEntityEvents(this, this.model, Marionette.getOption(this, "modelEvents"));
            Marionette.bindEntityEvents(this, this.collection, Marionette.getOption(this, "collectionEvents"));
            Marionette.bindEntityEvents(this, this.permission, Marionette.getOption(this, "permissionEvents"));
        },

        // Overriding Marionette's undelegateEvents to handle unbinding
        // the `triggers`, `modelEvents`, and `collectionEvents`, 'permissionEvents' config
        undelegateEvents: function(){
            var args = Array.prototype.slice.call(arguments);
            Backbone.View.prototype.undelegateEvents.apply(this, args);

            Marionette.unbindEntityEvents(this, this.model, Marionette.getOption(this, "modelEvents"));
            Marionette.unbindEntityEvents(this, this.collection, Marionette.getOption(this, "collectionEvents"));
            Marionette.unbindEntityEvents(this, this.permission, Marionette.getOption(this, "permissionEvents"));
        }
    });
});
