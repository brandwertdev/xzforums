define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        vote: function (postId, vote, callback, context) {
            var data = {
                vote: vote
            };

            $.ajax({
                url: Util.getApiUrl('post/vote/' + postId),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),
                beforeSend: function (xhr){
                    xhr.setRequestHeader('Authorization', 'BasicCustom');
                },
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        follow: function (postId, follow, callback, context) {
            var data = {
                is_follow: follow
            };

            $.ajax({
                url: Util.getApiUrl('post/' + postId + "/follow"),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),

                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        lock: function (postId, callback, context) {
            $.ajax({
                url: Util.getApiUrl('post/' + postId + "/lock"),
                type: 'PUT',

                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        sticky: function (postId, callback, context) {
            $.ajax({
                url: Util.getApiUrl('post/' + postId + "/sticky"),
                type: 'PUT',

                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        add:function(collection, post, callback){
//            collection.create(post,
//                {
//                    type: "post",
//                    url: this.url,
//                    success: function(){
//                        callback();
//                    }
//                });
        },

        update:function(post, attr, callback){
//            post.save(attr,
//                {
//                    type: "PUT",
//                    url: this.url,
//                    success: function(model, response){
//                        callback(response)
//                    }
//                });
        },

        getPostById:function(post, callback){
//            post.fetch(
//                {
//                    url: this.url,
//                    success: function(model, response){
//                        callback(model,response)
//                    }
//                });
        },

        delete:function(post, options){
            var url = post.url() + '/' + post.getId();

            post.destroy({
                url: url,
                success: function(model, response){
                    options.success(model, response);
                },
                error: function (model, xhr, ops) {
                    options.error(model, xhr, ops);
                }
            })
        }
    };

});