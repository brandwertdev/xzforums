define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        getSignature: function (callback, context) {
            return $.ajax({
                url: Util.getApiUrl('media/signature') + '?type=avatar_web',
                type: 'GET',
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        }
    };
});
