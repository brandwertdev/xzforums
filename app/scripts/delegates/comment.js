define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        delete:function(comment, options){
            var url = comment.url() + '/' + comment.getId(),
            ops = options || {};

            ops.url = url;

            comment.destroy(ops);
        },

        vote: function (commentId, vote, callback, context) {
            var data = {
                vote: vote
            };

            $.ajax({
                url: Util.getApiUrl('comment/vote/' + commentId),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),
                beforeSend: function (xhr){
                    xhr.setRequestHeader('Authorization', 'BasicCustom');
                },
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },
    };

});