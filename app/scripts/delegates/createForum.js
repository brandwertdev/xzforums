define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        checkForumSlug: function (forumSlug, callback, context) {
            return $.ajax({
                url: Util.getApiUrl('forum_slug_check') + '/' + forumSlug,
                type: 'GET',
                global: false,
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        submit: function (data, options, context) {
            return $.ajax({
                url: Util.getApiUrl('forums'),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),
                success: function(data, textStatus, XMLHttpRequest){
                    options.success&&options.success(context||this,[data, textStatus, XMLHttpRequest]);
                },
                error: function (jqHXR, textStatus) {
                    options.error&&options.error(context||this,[jqHXR, textStatus]);
                }
            });
        }
    };
});
