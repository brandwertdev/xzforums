define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        login: function (identity, password, options, context) {
            var data = {
                identity: identity,
                password: password
            };

            return $.ajax({
                url: Util.getApiUrl('password_authentication/login'),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),
                success: function(data, textStatus, XMLHttpRequest){
                    options.success&&options.success(context||this,[data, textStatus, XMLHttpRequest]);
                },
                error: function (jqHXR, textStatus) {
                    options.error&&options.error(context||this,[jqHXR, textStatus]);
                }
            });
        }
    };
});
