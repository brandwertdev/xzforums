define([
    'utils/util',

    'controllers/ajaxHandler'
], function(Util) {
    'use strict';

    return {
        checkIdentity: function (username, callback, context) {
            return $.ajax({
                url: Util.getApiUrl('register_check') + '/' + username,
                type: 'GET',
                global: false,
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        checkDisplayName: function (displayName, callback, context) {
            return $.ajax({
                url: Util.getApiUrl('display_name_check') + '/' + displayName,
                type: 'GET',
                global: false,
                success: function(data, textStatus, XMLHttpRequest){
                    callback&&callback.apply(context||this,[data, textStatus, XMLHttpRequest]);
                }
            });
        },

        register: function (identity, password, options, context) {
            var data = {
                identity: identity,
                password: password
            };

            return $.ajax({
                url: Util.getApiUrl('password_authentication/register'),
                type: 'POST',
                contentType: "application/json;",
                data: JSON.stringify(data),
                success: function(data, textStatus, XMLHttpRequest){
                    options.success&&options.success(context||this,[data, textStatus, XMLHttpRequest]);
                },
                error: function (jqHXR, textStatus) {
                    options.error&&options.error(context||this,[jqHXR, textStatus]);
                }
            });
        },

        updateDisplayName: function (displayName, options, context) {
            var data = {
                display_name: displayName
            };

            return $.ajax({
                url: Util.getApiUrl('user_profile'),
                type: 'PUT',
                contentType: "application/json;",
                data: JSON.stringify(data),
                success: function(data, textStatus, XMLHttpRequest){
                    options.success&&options.success(context||this,[data, textStatus, XMLHttpRequest]);
                },
                error: function (jqHXR, textStatus) {
                    options.error&&options.error(context||this,[jqHXR, textStatus]);
                }
            });
        },

        updateAvatar: function (avatarId, options, context) {
            var data = {
                avatar_id: avatarId
            };

            return $.ajax({
                url: Util.getApiUrl('user_profile'),
                type: 'PUT',
                contentType: "application/json;",
                data: JSON.stringify(data),
                success: function(data, textStatus, XMLHttpRequest){
                    options.success&&options.success(context||this,[data, textStatus, XMLHttpRequest]);
                },
                error: function (jqHXR, textStatus) {
                    options.error&&options.error(context||this,[jqHXR, textStatus]);
                }
            });
        }
    };
});
