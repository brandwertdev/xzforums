define([
    'backbone',

    'utils/util'
], function (Backbone, Util) {
    'use strict';

    var Forum = Backbone.Model.extend({

        idAttribute: 'entity_id',

        setId: function (id) {
            this.set('entity_id', id);
            return this;
        },

        getId: function () {
            return this.get('entity_id');
        },

        setTitle: function (title) {
            this.set('title', title);
            return this;
        },

        getTitle: function () {
            return this.get('title');
        },

        setLevel: function (level) {
            this.set('level', level);
            return this;
        },

        getLevel: function () {
            return this.get('level');
        }
    });

    return Forum;
});