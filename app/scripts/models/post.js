define([
    'backbone',

    'utils/util'
], function (Backbone, Util) {
    'use strict';

    var Post = Backbone.Model.extend({

        idAttribute: 'entity_id',

        url: function() {
            return Util.getApiUrl('post');
        },

        getTitle: function () {
            return this.get('title');
        },

        setTitle: function (title) {
            this.set('title', title);
            return this;
        },

        getAuthor: function () {
            return this.get('author_name');
        },

        getId: function () {
            return this.get('entity_id');
        },

        isFollowed: function () {
            return this.get('is_followed');
        },

        setFollowed: function (isFollowed) {
            this.set('is_followed', isFollowed);
            return this;
        },

        getFollowCount: function () {
            return this.get('follower_count');
        },

        setFollowCount: function (count) {
            this.set('follower_count', count);
            return this;
        },

        getCommentsCount: function () {
            return this.get('comments_count');
        },

        setCommentsCount: function (count) {
            this.set('comments_count', count);
            return this;
        },

        getMyVote: function () {
            return this.get('my_vote_number');
        },

        setMyVote: function (myVote) {
            this.set('my_vote_number', myVote);
            return this;
        },

        getPoints: function () {
            return this.get('points');
        },

        setPoints: function (point) {
            this.set('points', point);
            return this;
        },

        getForumSlug: function () {
            return this.get('forum_slug');
        },

        setForumSlug: function (forumSlug) {
            this.set('forum_slug', forumSlug);
        },

        getForumName: function () {
            return this.get('forum_name');
        },

        getActor: function () {
            return this.get('actor');
        },

        setLock: function (isLock) {
            this.set('lock', isLock);
            return this;
        },

        isLock: function () {
            return this.get('lock');
        },

        setSticky: function (isSticky) {
            this.set('sticky', isSticky);
            return this;
        },

        isSticky: function () {
            return this.get('sticky');
        }
    });

    return Post;
});