define([
    'backbone'
], function (Backbone) {
    'use strict';

    var Forum = Backbone.Model.extend({

        idAttribute: 'entity_id',

        setId: function (id) {
            this.set('entity_id', id);
            return this;
        },

        getId: function () {
            return this.get('entity_id');
        }
    });

    return Forum;
});