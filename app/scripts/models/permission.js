define([
    'backbone'
], function (Backbone) {
    'use strict';

    var Permission = Backbone.Model.extend({

        idAttribute: 'entity_id'
    });

    return Permission;
});