define([
    'backbone',

    'utils/util'
], function (Backbone, Util) {
    'use strict';

    var Comment = Backbone.Model.extend({

        idAttribute: 'entity_id',

        url: function() {
            return Util.getApiUrl('comment');
        },

        getId: function () {
            return this.get('entity_id');
        },

        setId: function (id) {
            this.set('entity_id', id);
            return this;
        },

        isActive: function () {
            return this.get('active');
        },

        setActive: function (isactive) {
            this.set('active', isactive);
            return this;
        },

        getBody: function () {
            return this.get('body');
        },

        getOperation: function () {
            return this.get('operation');
        },

        getNum: function () {
            return this.get('number');
        },

        getUnit: function () {
            return this.get('unit');
        },

        setForumSlug: function (forumSlug) {
            this.set('forum_slug', forumSlug);
            return this;
        },

        getForumSlug: function () {
            return this.get('forum_slug');
        },

        setPostId: function (postId) {
            this.set('post_id', postId);
            return this;
        },

        getPostId: function () {
            return this.get('post_id');
        },

        setPostSlug: function (postSlug) {
            this.set('post_slug', postSlug);
            return this;
        },

        getPostSlug: function () {
            return this.get('post_slug');
        },

        getMyVote: function () {
            return this.get('my_vote_number');
        },

        setMyVote: function (myVote) {
            this.set('my_vote_number', myVote);
            return this;
        },

        getPoints: function () {
            return this.get('points');
        },

        setPoints: function (point) {
            this.set('points', point);
            return this;
        }
    });

    return Comment;
});