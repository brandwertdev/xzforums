define([
    'marionette'
], function(){
    "use strict";

    return new Backbone.Wreqr.EventAggregator();
});