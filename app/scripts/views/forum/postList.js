define([
    'lodash',
    'marionette',

    'vent',
    'utils/util',

    'models/post',

    'views/post/postItem',
    'views/util/loading',
    'views/util/loadingEnd',

    'controllers/pullRefresh'
], function (_, Marionette, vent, Util, Post, PostItemView, Loading, LoadingEnd, PullRefresh) {
    'use strict';

    var PostList = Marionette.CollectionView.extend({

        ops: {
            inited: false,
            forumId: 0,
            pageSize: 10,
            page: 1,
            postFetching: false,
            postEnd: false,
            filter: 'all',
            sort: 'hotness'
        },

        ui: {
            listview: 'ul[data-role=listview]',
            pagination: '.pagination'
        },

        itemView: PostItemView,

        itemViewOptions: function () {
            return {
                belongPage: this.ops.page
            };
        },

        initialize: function () {
            var self = this;
            console.log('initialize PostList.');

            vent.on('postList:filter', _.bind(this.filter, self));
            vent.on('postList:sort', _.bind(this.sort, self));
        },

        onClose: function () {
            vent.off('postList:filter');
            vent.off('postList:sort');
        },

        attachToView: function () {
            var self = this;

            this.$el = $('#content');
            this.el = this.$el[0]

            this.$el.addClass('pull-refresh-wrapper');

            this.bindUIElements();

            this.$('.post-item').each(function() {
                var postEl = $(this),
                    post = new Post(),
                    postItem;

                self.collection.add(post, {silent: true});

                postItem = new self.itemView({
                    model: post,
                    el: postEl
                });

                self.children.add(postItem);

                postItem.attachToView();
            });

            this.ops.page = parseInt(this.ui.listview.attr('data-page'));
            this.ops.forumId = parseInt(this.ui.listview.attr('data-id'));

            this.triggerMethod('render');
        },

        onRender: function () {
            console.log('PostList rendered.');

            if (!this.ops.inited) {
                this.attachEndlessScroll();

                this.ops.inited = true;
            }
        },

        appendHtml: function (collectionView, itemView, index) {
            this.ui.listview.append(itemView.el);
        },

        attachEndlessScroll: function () {
            var self = this;
//            $(window).endlessScroll({
//                bottomPixels: 200,
//                fireDelay: 10,
//                fireOnce: true,
//                loader: "",
//                callback: function(){
//                    if($(window).height() < self.$el.height() && self.ops.postFetching == false && !self.ops.postEnd){
//                        self.loadNewPosts();
//                    }
//                }
//            });

            this.pullRefresh = new PullRefresh({
                wrapper: $('.pull-refresh-wrapper'),
                load: function (dtd) {
                    $(self.loadNewPosts(dtd));
                }
            });
        },

        loadNewPosts: function(defer) {
            var self = this;
            this.ops.page++;

            this.fetchPosts({
                add: true,
                merge: false,
                remove: false,

                success: function(collection, resp, ops){
                    if (!resp.pagination['has_next']) {
                        var loadingEnd = new LoadingEnd({
                            className: 'post-item text-center',
                            tagName: 'li'
                        });

                        loadingEnd.render();

                        self.ops.postEnd = true;

                        self.ui.listview.append(loadingEnd.el);

                        self.listenTo(loadingEnd, 'scrollTop', self.scrollTop);

                        self.pullRefresh.setLoadEnd(true);
                    }

                    self.ui.listview.listview('refresh').trigger('create');

                    defer.resolve();
                },

                error: function () {
                    self.ops.page--;

                    defer.reject();
                }
            });
        },

        fetchPosts: function (options) {
            var self = this, ops = {};

            _.extend(ops, options);

            ops.success = function(collection, resp, ops){
                options.success && options.success(collection, resp, ops);
                self.ops.postFetching = false;
            };

            ops.error = function(collection, resp, ops){
                options.error && options.error(collection, resp, ops);
                self.ops.postFetching = false;
            };

            if (!_.has(ops, 'data'))
                ops.data = {
                    page: self.ops.page,
                    page_size: self.ops.pageSize,
                    forum_id: self.ops.forumId,
                    filter: self.ops.filter,
                    order_by: self.ops.sort
                };

            this.ops.postFetching = true;

            this.collection.fetch(ops);
        },

        scrollTop: function () {
            this.pullRefresh.scrollTo(0, 0, 400);
        },

        filter: function (type) {
            var self = this;

            if (this.ops.filter == type)
                return null;

            this.ui.listview.html('');

            this.ops.filter = type;
            this.ops.page = 1;

            this.fetchPosts({
                reset: true,

                success: function(){
                    self.ui.listview.listview('refresh').trigger('create');
                }
            });
        },

        sort: function (type) {
            var self = this;

            if (this.ops.sort == type)
                return null;

            this.ui.listview.html('');

            this.ops.sort = type;
            this.ops.page = 1;

            this.fetchPosts({
                reset: true,

                success: function(){
                    self.ui.listview.listview('refresh').trigger('create');
                }
            });
        }
    });

    return PostList;
});