define([
    'lodash',
    'marionette',
    'vent',

    'collections/forumList',
    'models/forum'
], function (_, Marionette, vent, ForumList, Forum) {
    var ForumComposite = Marionette.CompositeView.extend({

        tapActive: false,
        ventNS: '',

        ops: {
            isItem: false
        },

        ui: {
            link: '.forum-link'
        },

        events: {
            'click .forum-link': 'clickForum'
        },

        modelEvents: {
        },

        initialize: function (options) {
            console.log('initialize ForumItem.');

            this.ops.isItem = this.$el.is('li');

            this.ops = _.extend({}, this.ops, this.options);

            if (this.ops.isItem && this.ops.tapActive) {
                vent.on('createpost:forum:inactive', _.bind(this.inactive, this));
            }
        },

        onClose: function () {
            if (this.ops.isItem) {
                vent.off('createpost:forum:inactive');
            }
        },

        delegateEvents: function () {
            if (this.ops.isItem)
                Marionette.View.prototype.delegateEvents.call(this);
        },

        bindUIElements: function () {
            if (this.ops.isItem)
                Marionette.View.prototype.bindUIElements.call(this);
        },

        attachToView: function () {
            var self = this;

            this.bindUIElements();
            this.delegateEvents();

            if (!this.ops.isItem) {
                this.$el.children().each(function () {
                    var forumEl = $(this),
                        forumList = new ForumList(),
                        forum = new Forum(), forumItem;

                    forumItem = new ForumComposite({
                        model: forum,
                        collection: forumList,
                        el: forumEl,
                        tapActive: self.ops.tapActive,
                        ventNS: self.ops.ventNS
                    });

                    forumItem.attachToView();
                });
            } else {
                this.initModel();
            }
        },

        initModel: function () {
            var id = this.$el.attr('data-id'),
                level = parseInt(this.$el.attr('data-level')),
                title = this.ui.link.text().trim();

            this.model.setId(id).setTitle(title).setLevel(level);
        },

        inactive: function () {
            var link = this.ui.link;

            if (link.hasClass('active'))
                link.removeClass('active');
        },

        clickForum: function (evt) {
            var model = this.model;

            evt.preventDefault();

            if (this.ops.tapActive) {
                vent.trigger('createpost:forum:inactive');
                this.ui.link.addClass('active');
            }

            vent.trigger(this.ops.ventNS + ':forum:picked', model.getLevel() + ':' + model.getId() + ':' + model.getTitle());
        }
    });

    return ForumComposite;
});
