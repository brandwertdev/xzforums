define([
    'lodash',
    'marionette',
    'vent'
], function (_, Marionette, vent) {
    'use strict';

    var PostListMenu = Marionette.ItemView.extend({

        ui: {
            filterGroup: '.filters div[data-role=controlgroup]',
            filterAll: '.filters .all',
            filterClan: '.filters .clan',
            filterMy: '.filters .my',
            sortGroup: '.sort div[data-role=controlgroup]',
            sortHotness: '.sort .hotness',
            sortNew: '.sort .created_at'
        },

        events: {
            'click .filters .all' : 'clickFilter',
            'click .filters .my' : 'clickFilter',
            'click .filters .clan' : 'clickFilter',
            'click .sort .hotness': 'clickSort',
            'click .sort .created_at': 'clickSort'
        },

        initialize: function (options) {
            console.log('initialize PostListMenu.');

            _.extend(this, options);
        },

        onClose: function () {
            //$(window).off('resize.postListMenu');
        },

        attachToView: function () {
            this.$el = $('#menu-panel');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.setFilters();

            this.triggerMethod('render');
        },

        setFilters: function () {
            if (this.ui.filterAll.hasClass('ui-btn-active'))
                this.ops.filter = 'all';
            else if (this.ui.filterMy.hasClass('ui-btn-active'))
                this.ops.filter = 'my';
        },

        clickFilter: function (evt) {
            var targetBtn;

            evt.preventDefault();

            targetBtn = $(evt.target).parents('a[data-role=button]');

            this.$el.panel('close');

            this.ui.filterGroup.find('a[data-role=button]').each(function (index, element) {
                var el = $(element);

                if (el[0] == targetBtn[0]) {
                    if (!targetBtn.hasClass('ui-btn-active'))
                        targetBtn.addClass('ui-btn-active');
                } else {
                    el.removeClass('ui-btn-active');
                }
            });

            vent.trigger('postList:filter', targetBtn.text().toLowerCase());
        },

        clickSort: function (evt) {
            var targetBtn;

            evt.preventDefault();

            targetBtn = $(evt.target).parents('a[data-role=button]');

            this.$el.panel('close');

            this.ui.sortGroup.find('a[data-role=button]').each(function (index, element) {
                var el = $(element);

                if (el[0] == targetBtn[0]) {
                    if (!targetBtn.hasClass('ui-btn-active'))
                        targetBtn.addClass('ui-btn-active');
                } else {
                    el.removeClass('ui-btn-active');
                }
            });

            vent.trigger('postList:sort', targetBtn.text().toLowerCase() == 'new' ? 'created_at' : targetBtn.text().toLowerCase());
        }
    });

    return PostListMenu;
});
