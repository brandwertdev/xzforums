define([
    'lodash',

    'marionette',

    'utils/util',
    'controllers/post',
    'controllers/vote',
    'controllers/follow',

    'delegates/post'
], function (_, Marionette, Util, PostController, VoteController, FollowController, Delegator){
    'use strict';

    var PostItem = Marionette.ItemView.extend({
        template: '#tplPostItem',

        tagName: 'li',
        className: 'post-item',

        initialize: function (options) {
            console.log('initialize PostItem.');

            _.extend(this, options);

            this.delegator = Delegator;
            this.addController([PostController, VoteController, FollowController]);
        },

        attachToView: function () {
            this.bindUIElements();

            this.initModel();
        },

        serializeData: function () {
            var post = this.model;

            _.extend(post, this.setRenderHelpers());

            return {
                post: post,
                slugFormat: Util.slugFormat,
                url: Util.getUrlPath()
            };
        }
    });

    return PostItem;
});