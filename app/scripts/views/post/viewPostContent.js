define([
    'lodash',
    'marionette',

    'controllers/vote',
    'controllers/follow',
    'controllers/lock',
    'controllers/sticky',

    'views/comment/commentComposite',

    'delegates/post'
], function (_, Marionette, VoteController, FollowController, LockController, StickyController, CommentCompositeView,
             Delegator) {
    'use strict';

    var ViewPostContent = Marionette.ItemView.extend({

        ui: {
            addCommentBtn: '.status .add-comment',
            voteUpDiv: '.vote .up',
            voteDownDiv: '.vote .down'
        },

        permissionEvents: {
            'change:canCreateComment': 'changeAddCommentPerm',
            'change:canVoteUpOnTopic': 'changeVoteUpPerm',
            'change:canVoteDownOnTopic': 'changeVoteDownPerm'
        },

        initialize: function () {
            console.log('initialize viewPostContent.');
            this.delegator = Delegator;
            this.addController([VoteController, FollowController, LockController, StickyController]);

            this.permission = this.options.permission;
            delete this.options.permission;
        },

        onClose: function () {

        },

        attachToView: function () {
            this.$el = $('#content .post');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.ui.comments = $('#content .comments>ul');

            if (!_.isEmpty(this.ui.comments)) {
                var commentsView = new CommentCompositeView({
                    el: this.ui.comments,
                    permission: this.permission
                });
                commentsView.attachToView();
            }
        },

        changeAddCommentPerm: function () {
            if (this.permission.get('canCreateComment'))
                this.ui.addCommentBtn.show();
            else
                this.ui.addCommentBtn.hide();
        },

        changeVoteUpPerm: function () {
            if (this.permission.get('canVoteUpOnTopic'))
                this.ui.voteUpDiv.show();
            else
                this.ui.voteUpDiv.hide();
        },

        changeVoteDownPerm: function () {
            if (this.permission.get('canVoteDownOnTopic'))
                this.ui.voteDownDiv.show();
            else
                this.ui.voteDownDiv.hide();
        }
    });

    return ViewPostContent;
});