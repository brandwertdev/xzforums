define([
    'lodash',

    'marionette',
    'vent',

    'views/forum/forumComposite'
], function (_, Marionette, Vent, ForumCompositeView) {
    var View = Marionette.ItemView.extend({

        ops: {
            validFields: {
                title: false,
                body: false
            }
        },

        ui: {
            title: 'input[name=title]',
            body: 'textarea[name=body]',
            forumId: '[name=forumId]',
            forumPicker: '.forum-picker',
            submit: 'input[type=submit]',
            cancel: '.cancel'
        },

        events: {
            'input input[name=title]': 'inputTitle',
            'input textarea[name=body]': 'inputBody',
            'click .forum-picker': 'pickForum',
            'click .cancel': 'goBack',
            'click [type=submit]': 'clickSubmit'
        },

        initialize: function () {
            console.log('initialize CreatePostContent.');

            Vent.on('createpost:forum:picked', _.bind(this.forumPicked, this));
        },

        onClose: function () {
            Vent.off('createpost:forum:picked');
        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.ui.forumList = $('#forum-list ul.top-level');

            if (!_.isEmpty(this.ui.forumList)) {
                var forumsView = new ForumCompositeView({
                    el: this.ui.forumList,
                    tapActive: true,
                    ventNS: 'createpost'
                });

                forumsView.attachToView();
            }

            this.initView();
        },

        initView: function () {
            var forumPage;

            if (this.ui.title.val())
                this.inputTitle();
            if (this.ui.body.val())
                this.inputBody();

            this.ui.forumListPage = {
                $el: $('#forum-list')
            };

            forumPage = this.ui.forumListPage;

            forumPage.backBtn = forumPage.$el.find('.back-btn');

            forumPage.backBtn.click(_.bind(this.backMainPage, this));
        },

        inputTitle: function () {
            var title = this.ui.title, val = title.val();

            if (val.length > 0 && val.length <= 512)
                this.ops.validFields.title = true;
            else
                this.ops.validFields.title = false;

            this.checkSubmit();
        },

        inputBody: function () {
            var body = this.ui.body, val = body.val();

            if (val.length > 0 && val.length <= 63206)
                this.ops.validFields.body = true;
            else
                this.ops.validFields.body = false;

            this.checkSubmit();
        },

        pickForum: function () {
            $.mobile.changePage(this.ui.forumListPage.$el);
        },

        forumPicked: function (forumStr) {
            var regexp = /^(\d+):(\d+):(.*)$/g, result;

            result = regexp.exec(forumStr);
            this.backMainPage();

            this.ui.forumId.val(result[2]);
            this.ui.forumPicker.find('.ui-btn-text').text(result[3]);
        },

        backMainPage: function () {
            $.mobile.changePage(this.$el.parents('[data-role=page]'));
        },

        checkSubmit: function () {
            if (this.ops.validFields.title && this.ops.validFields.body)
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        clickSubmit: function () {
            this.disableSubmit();
        },

        goBack: function () {
            history.back();
        },

        disableSubmit: function () {
            this.ui.submit.parent().removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submit.parent().addClass('active');
        }
    });

    return View;
});