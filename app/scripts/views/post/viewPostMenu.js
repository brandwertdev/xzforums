define([
    'lodash',
    'marionette',

    'vent',

    'utils/util',
    'delegates/post',
    'controllers/post',
    'controllers/vote',
    'controllers/follow',
    'controllers/lock',
    'controllers/sticky'
], function (_, Marionette, vent, Util, Delegator, PostController, VoteController, FollowController, LockController,
             StickyController) {
    'use strict';

    var ViewPostMenu = Marionette.ItemView.extend({

        ui: {
            delete: '.action .delete',
            editBtn: '.action .edit',
            addCommentBtn: '.status .add-comment',
            voteUpDiv: '.vote .up',
            voteDownDiv: '.vote .down'
        },

        events: {
            'click .action .delete': 'clickDelete'
        },

        permissionEvents: {
            'change:canUpdatePost': 'changeUpdatePostPerm',
            'change:canDeletePost': 'changeDeletePostPerm',
            'change:canCreateComment': 'changeAddCommentPerm',
            'change:canVoteUpOnTopic': 'changeVoteUpPerm',
            'change:canVoteDownOnTopic': 'changeVoteDownPerm'
        },

        initialize: function () {
            console.log('initialize ViewPostMenu.');
            this.delegator = Delegator;
            this.addController([PostController, VoteController, FollowController, LockController, StickyController]);

            vent.on('viewPost:comment:delete', _.bind(this.commentDelete, this));

            this.permission = this.options.permission;

            delete this.options.permission;
        },

        onClose: function () {
            vent.off('viewPost:comment:delete');
        },

        attachToView: function () {
            this.$el = $('#view-post-menu');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initModel();

            this.model.setForumSlug(this.$el.attr('data-forum'));

            this.initView();
            this.initPermission();
        },

        initView: function () {
        },

        initPermission: function () {
            if (this.ui.addCommentBtn.hasClass('ui-hidden'))
                this.permission.set('canCreateComment', false, {silent: true});
            else
                this.permission.set('canCreateComment', true, {silent: true});
        },

        clickDelete: function (evt) {
            evt.preventDefault();

            Util.dialog({
                header: 'Delete Post',
                content: 'Are you sure you want to delete this post?',
                button: {
                    text: 'Delete',
                    callback: _.bind(this.actualDelete, this)
                }
            });
        },

        actualDelete:function () {
            window.location.pathname = this.ui.delete.attr('href');
        },

        commentDelete: function () {
            this.model.setCommentsCount(this.model.getCommentsCount() - 1);
        },

        lockCallback: function (data, textStatus, XMLHttpRequest) {
            var permission = data.post.controller;
            this.permission.set(permission);
        },

        changeUpdatePostPerm: function () {
            if (this.permission.get('canUpdatePost'))
                this.ui.editBtn.show();
            else
                this.ui.editBtn.hide();
        },

        changeDeletePostPerm: function () {
            if (this.permission.get('canDeletePost'))
                this.ui.delete.show();
            else
                this.ui.delete.hide();
        },

        changeAddCommentPerm: function () {
            if (this.permission.get('canCreateComment'))
                this.ui.addCommentBtn.show();
            else
                this.ui.addCommentBtn.hide();
        },

        changeVoteUpPerm: function () {
            if (this.permission.get('canVoteUpOnTopic'))
            this.ui.voteUpDiv.show();
            else
            this.ui.voteUpDiv.hide();
        },

        changeVoteDownPerm: function () {
            if (this.permission.get('canVoteDownOnTopic'))
                this.ui.voteDownDiv.show();
            else
                this.ui.voteDownDiv.hide();
        }
    });

    return ViewPostMenu;
});