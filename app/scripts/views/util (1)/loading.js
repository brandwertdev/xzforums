define([
    'marionette'
],function(Marionette) {
    'use strict';

    var Loading = Marionette.ItemView.extend({
        template: '',

        tagName: 'div',

        className: 'loading'
    });

    return Loading;
});