define([
    'lodash',
    'backbone',
    'marionette'
], function (_, Backbone, Marionette){
    'use strict';

    var Header = Marionette.ItemView.extend({
//        el: '#header-container',
//
//        ui: {
//            leftToggle: 'a[data-action=toggle-left-nav]',
//            rightToggle: 'a[data-action=toggle-right-nav]',
//            mainMenu: '.headerbar-expand'
//        },
//
//        events: {
//            'click a[data-action=toggle-left-nav]': 'toggleLeftNav',
//            'click a[data-action=toggle-right-nav]': 'toggleRightNav'
//        },
//
//        initialize: function(){
//            this.bindUIElements();
//            this.setContentMenuHeight();
//
//            $(window).on('resize.header', _.bind(this.onWindowResize, this));
//        },
//
//        onClose: function () {
//            $(window).off('resize.header');
//        },
//
//        toggleLeftNav: function () {
//            if (this.ui.leftToggle.hasClass('active')) {
//                this.ui.leftToggle.removeClass('active');
//                this.leftAnitmation(false);
//            } else {
//                this.ui.leftToggle.addClass('active');
//                this.ui.mainMenu.show();
//                this.leftAnitmation(true);
//            }
//        },
//
//        leftAnitmation: function (isExpand) {
//            var windowWidth = $(window).width();
//
//            if (windowWidth < 768) {
//                $('#content, #header').animate({marginLeft: isExpand ? '50%' : '0%'}, 200);
//                this.ui.mainMenu.animate({marginLeft: isExpand ? '0%' : '-50%'}, 200);
//            } else {
//                $('#content, #header').animate({marginLeft: isExpand ? '250px' : '0px'}, 200);
//                this.ui.mainMenu.animate({marginLeft: isExpand ? '0' : '-250px'}, 200);
//            }
//        },
//
//        toggleRightNav: function () {
//            var contentMenuEl = $('#contentmenu');
//
//            if (this.ui.rightToggle.hasClass('active')) {
//                this.ui.rightToggle.removeClass('active');
//                this.rightAnitmation(false);
//            } else {
//                this.ui.rightToggle.addClass('active');
//                contentMenuEl.show();
//                this.rightAnitmation(true);
//            }
//        },
//
//        rightAnitmation: function (isExpand) {
//            var windowWidth = $(window).width(),
//                contentMenuEl = $('#contentmenu');
//
//            if(windowWidth < 768){
//                $('#content, #header').animate({marginLeft: isExpand ? '-75%' : '0%'}, 200);
//                contentMenuEl.animate({marginLeft: isExpand ? '-75%' : '0%'}, 200);
//            }else{
//                $('#content, #header').animate({marginLeft: isExpand ? '-360px' : '0px'}, 200);
//                contentMenuEl.animate({marginLeft: isExpand ? '-360px' : '0px'}, 200);
//            }
//        },
//
//        onWindowResize: function () {
//            this.ui.leftToggle.removeClass('active');
//            this.ui.rightToggle.removeClass('active');
//            this.ui.mainMenu.attr('style', '');
//            $('#content, #header, #contentmenu').attr('style', '');
//            this.setContentMenuHeight();
//        },
//
//        setContentMenuHeight: function () {
//            var contentMenuEl = $('#contentmenu');
//
//            if (!_.isEmpty(contentMenuEl)) {
//                contentMenuEl.height($(window).height() - 65);
//            }
//        }
    });

    return Header;
});