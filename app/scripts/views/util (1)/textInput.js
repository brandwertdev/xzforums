define([
    'marionette',

    'utils/util'
],function(Marionette, Util) {

    var textInput = Marionette.ItemView.extend({
        template: '',

        alertIconStr: '<a data-rel="popup" data-position-to="window" data-role="button" data-iconpos="notext" data-icon="alert" class="helper-icon alert"></a>',
        successIconStr: '<a data-role="button" data-iconpos="notext" data-icon="check" class="helper-icon success"></a>',

        _isValid: false,

        events: {
            'input': 'input'
        },

        initialize: function() {
            console.log('initialize TextInput');
        },

        initView: function () {
            var self = this,
                parentEl = this.$el.parent(),
                serverErr = parentEl.next().text().trim(),
                clearBtn = parentEl.find('.ui-input-clear');
            if (serverErr) {
                this.setFail(serverErr);
            } else if (this.getVal()) {
                this.input();
            }

            clearBtn.click(function () {
                self.input();
                parentEl.find('.helper-icon').hide();
            });
        },

        input: function () {
            var field = this.$el,
                value = field.val(),
                errStr = '',
                validators = this.options.validators;

            for (var v in validators) {
                if (!validators[v].validate(value)) {
                    if (!!errStr)
                        errStr += '<br>';

                    errStr += validators[v].msg;
                }
            }

            if (!errStr)
                this.setSuccess();
            else
                this.setFail(errStr);

            this.options.inputCallback && this.options.inputCallback();
        },

        setSuccess: function () {
            var success, alert, parent = this.$el.parent();

            if ((alert=parent.find('.helper-icon.alert')).length != 0) {
                alert.hide();
            }

            if ((success=parent.find('.helper-icon.success')).length == 0) {
                success = $(this.successIconStr).button();
                this.$el.after(success);
            } else {
                success.show();
            }

            this._isValid = true;
        },

        setFail: function (errStr) {
            var self= this, success, alert, parent = this.$el.parent();

            if ((success=parent.find('.helper-icon.success')).length != 0) {
                success.hide();
            }

            self.errStr = errStr;

            if ((alert=parent.find('.helper-icon.alert')).length == 0) {
                alert = $(this.alertIconStr).button();

                alert.click(function () {
                    Util.popup('<p>' + self.errStr + '</p>');
                });

                this.$el.after(alert);
            } else {
                alert.show();
            }

            this._isValid = false;
        },

        isValid: function () {
            return this._isValid;
        },

        getVal: function () {
            return this.$el.val().trim();
        }
    });

    return textInput;
});