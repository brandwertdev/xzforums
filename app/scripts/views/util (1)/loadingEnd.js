define([
    'marionette'
],function(Marionette) {

    var LoadingEnd = Marionette.ItemView.extend({
        template: '#loadingEnd',

        events: {
            "click .top": "scrollTop"
        },

        initialize: function() {

        },

        scrollTop: function(evt){
            evt.preventDefault();

            this.triggerMethod('scrollTop');
        }
    });

    return LoadingEnd;
});