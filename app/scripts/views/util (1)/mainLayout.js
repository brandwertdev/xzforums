define([
    'marionette'
], function (Marionette) {
    var MainLayout = Marionette.Layout.extend({

        regions: {
            mainContent: '#content',
            contentMenu: '#menu-panel'
        },

        initialize: function () {
            console.log('initialize MainLayout.');

            this.initPanels();
        },

        initPanels: function () {
            var rightPanel;

            if ($('#nav-panel').length)
                $('#nav-panel').panel({
                    beforeopen: function () {
                        $('#content').parent().css('position', 'fixed');
                    },
                    beforeclose: function () {
                        $('#content').parent().css('position', 'relative');
                    }
                });

            if ((rightPanel = $('[data-role=panel][data-position=right]')).length)
                rightPanel.panel({
                    beforeopen: function () {
                        $('#content').parent().css('position', 'fixed');
                    },
                    beforeclose: function () {
                        $('#content').parent().css('position', 'relative');
                    }
                });
        }
    });

    return MainLayout;
});