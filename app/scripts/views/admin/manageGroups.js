define([
    'lodash',
    'marionette',

    'utils/util'
], function (_, Marionette, Util) {
    'use strict';

    var View = Marionette.ItemView.extend({

        template: '',

        actionPopTpl:
            '<div class="action-sheet">' +
                '<a data-role="button" data-rel="back">Cancel</a>' +
                '<a data-role="button" href="/admin/create_group/">Create a new group and copy my permission</a>' +
                '<a data-role="button" href="/admin/edit_group/">Edit group name</a>' +
                '<a data-role="button" href="#">Delete group</a>' +
                '</div>',

        //deleteBtnTpl: '<a data-role="button" href="/admin/delete_forum">Delete</a>',

        ui: {

        },

        events: {
            'click .role-item': 'clickRoleItem'
        },

        initialize: function () {
            console.log('initialize ManageGroups.');
        },

        onClose: function () {
        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.triggerMethod('render');
        },

        onRender: function () {
        },

        clickRoleItem: function (evt) {
            var tplStr, popEl;

            evt.preventDefault();

            tplStr = _.template(this.actionPopTpl, {});

            popEl = $(tplStr);

            Util.showMenu(popEl);
        }
    });

    return View;
});