define([
    'lodash',
    'marionette',

    'models/permission',

    'utils/util'
], function (_, Marionette, Permission, Util) {
    'use strict';

    var View = Marionette.ItemView.extend({

        template: '',

        ui: {
            submitBtn: '.ui-submit',
            form: 'form'
        },

        events: {
            'click .cancel': 'goBack',
            'click .ui-submit': 'submit',
            'change select': 'changePermission'
        },

        initialize: function () {
            console.log('initialize EditPermission.');
        },

        onClose: function () {
        },

        attachToView: function () {
            this.$el = $('#editPermissionForm');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initModel();

            this.triggerMethod('render');
        },

        initModel: function () {
            var list = this.$el.find('ul.edit-permission li'),
                data = {};

            list.each(function (index, elem) {
                var select = $(elem).find('select');
                data[select.attr('name')] = select.val();
            });

            this.model = new Permission(data);
            this.model.origin = data;
        },

        onRender: function () {
        },

        goBack: function () {
            history.back();
        },

        submit: function (evt) {
            var self = this;

            evt.preventDefault();

            Util.dialog({
                header: 'Save Permission',
                content: '<strong>Are you sure?</strong><br>This change cannot be undone.',
                button: {
                    text: 'Yes, make the change',
                    callback: function (dialog) {
                        dialog.popup('close');
                        self.$el.submit();
                        self.disableSubmit();
                    }
                }
            });
        },

        changePermission: function (evt) {
            var elem = $(evt.target),
                name = elem.attr('name'),
                val = elem.val();

            this.model.set(name, val);

            this.checkSubmit();
        },

        disableSubmit: function () {
            this.ui.submitBtn.removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submitBtn.addClass('active');
        },

        checkSubmit: function () {
            if (this.hasChanged())
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        hasChanged: function () {
            var model = this.model,
                current = model.attributes,
                origin = model.origin, attr, val;

            for (attr in current) {
                val = current[attr];
                if (!_.isEqual(origin[attr], val))
                    return true;
            }

            return false;
        }
    });

    return View;
});