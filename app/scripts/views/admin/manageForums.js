define([
    'lodash',
    'marionette',

    'vent',

    'utils/util',

    'views/forum/forumComposite'
], function (_, Marionette, vent, Util, ForumCompositeView) {
    'use strict';

    var View = Marionette.ItemView.extend({

        template: '',

        actionPopTpl:
            '<div class="action-sheet">' +
                '<a data-role="button" data-rel="back">Cancel</a>' +
                '<a data-role="button" href="/admin/forum_permission?forumId=<%=forumId%>">Manage permission</a>' +
                '<a data-role="button" href="/admin/create_forum?forumId=<%=forumId%>">Create a subforum</a>' +
                '<a data-role="button" href="/admin/edit_forum">Edit</a>' +
            '</div>',

        deleteBtnTpl: '<a data-role="button" href="/admin/delete_forum">Delete</a>',

        ui: {
            forumList: '.forum-list ul.top-level'
        },

        initialize: function () {
            console.log('initialize ManageForums.');

            vent.on('manageforums:forum:picked', _.bind(this.clickForum, this));
        },

        onClose: function () {
            vent.off('manageforums:forum:picked');
        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            if (!_.isEmpty(this.ui.forumList)) {
                var forumsView = new ForumCompositeView({
                    el: this.ui.forumList,
                    ventNS: 'manageforums'
                });

                forumsView.attachToView();
            }

            this.triggerMethod('render');
        },

        onRender: function () {
        },

        clickForum: function (forumStr) {
            var regexp = /^(\d+):(\d+):(.*)$/g,
                result, tplStr, popEl;

            result = regexp.exec(forumStr);

            tplStr = _.template(this.actionPopTpl, {forumId: result[2]});

            popEl = $(tplStr);

            if (result[1] != '0')
                popEl.append(this.deleteBtnTpl);


            Util.showMenu(popEl);
        }
    });

    return View;
});