define([
    'lodash',
    'marionette',

    'vent',

    'delegates/createForum',

    'views/util/textInput',
    'views/forum/forumComposite'
], function (_, Marionette, Vent, Delegator, TextInput, ForumCompositeView) {
    'use strict';

    var View = Marionette.ItemView.extend({

        template: '',

        ui: {
            form: 'form',
            copyFrom: '[name="copyFrom"]',
            submit: 'input[type=submit]'
        },

        events: {
            'click [data-role=cancel]': 'goBack',
            'click input:submit': 'submit',
            'blur input[name=forumSlug]': 'checkForumSlug'
        },

        initialize: function () {
            console.log('initialize CreateForum.');

            Vent.on('createforum:forum:picked', _.bind(this.forumPicked, this));
        },

        onClose: function () {
            Vent.off('createforum:forum:picked');
        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initView();

            this.triggerMethod('render');
        },

        initView: function () {
            this.ui = this.ui || {};

            this.ui.forumList = $('#forum-list ul.top-level');

            if (!_.isEmpty(this.ui.forumList)) {
                var forumsView = new ForumCompositeView({
                    el: this.ui.forumList,
                    tapActive: false,
                    ventNS: 'createforum'
                });

                forumsView.attachToView();
            }

            this.ui.forumName = new TextInput({
                el: this.$el.find('input[name=forumName]'),
                validators: this.getValidators(),
                inputCallback: _.bind(this.checkNext, this)
            });

            this.ui.forumSlug = new TextInput({
                el: this.$el.find('input[name=forumSlug]')
            });

            this.ui.forumName.initView();
            this.ui.forumSlug.initView();

            this.parent_id = parseInt(this.$el.find('form').find('[name=parentId]').val());

            this.ui.forumListPage = {
                $el: $('#forum-list')
            };
        },

        getValidators: function () {
            return [{
                validate: function (val) {
                    return val.length > 0 && val.length <= 512;
                },
                msg: 'Forum name has to be less than 512 characters.'
            }, {
                validate: function (val) {
                    return /^[a-z\s]*$/ig.test(val);
                },
                msg: 'Invalid character, forum name can only contain characters and space.'
            }];
        },

        checkNext: function () {
            if (this.ui.forumName.isValid())
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        onRender: function () {
        },

        checkForumSlug: function () {
            var self = this,
                forumSlug = this.ui.forumSlug, val;

            if (forumSlug.getVal()) {
                val = forumSlug.getVal();

                Delegator.checkForumSlug(val, function (data) {
                    if (data && data.exist_result && data.exist_result.exist) {
                        self.ui.forumSlug.setFail('Sorry, forum slug is taken.');
                        self.disableSubmit();
                    }
                });
            }
        },

        disableSubmit: function () {
            this.ui.submit.parent().removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submit.parent().addClass('active');
        },

        submit: function (evt) {
            evt.preventDefault();

            $.mobile.changePage(this.ui.forumListPage.$el);
        },

        goBack: function () {
            history.back();
        },

        forumPicked: function (forumStr) {
            var regexp = /^(\d+):(\d+):(.*)$/g, result;

            result = regexp.exec(forumStr);
            this.ui.copyFrom.val(result[2]);

            this.ui.form.submit();
        }
    });

    return View;
});