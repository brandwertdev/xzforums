define([
    'marionette',

    'delegates/register',

    'controllers/uploadImage'
], function (Marionette, Delegator, uploadImageController) {
    'use strict';

    var View = Marionette.ItemView.extend({
        ui: {
        },

        events: {
            'click a[role=avatar]': 'submit',
            'click .upload': 'showPreview'
        },

        initialize: function () {
            console.log('initialize PickAvatar.');

            this.addController(uploadImageController);
        },

        onClose: function () {
        },

        attachToView: function () {
            this.$el = $('#pickAvatarForm');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initModel();
        },

        initModel: function () {
            var id = this.$el.attr('data-id');

            this.model.setId(id);
        },

        submit: function (evt) {
            evt.preventDefault();

            var id = $(evt.target).parents('li').attr('data-id'), self = this;

            Delegator.updateAvatar(id, {
                success: function () {
                    self.forward();
                },
                error: function (jqXHR, textStatus) {
                    if (textStatus[0]) {
                        alert('err');
                    }
                }
            });
        },

        forward: function () {
            var nextReg = /[\?&]?next=([^&]+)/ig,
                result;

            if (location.search.trim()) {
                result = nextReg.exec(location.search.trim());
            }

            if (result && result[1]) {
                window.location = unescape(result[1]);

                return true;
            }

            window.location = '/';
        },

        uploadSuccess: function () {
            this.forward();
        }
    });

    return View;
});