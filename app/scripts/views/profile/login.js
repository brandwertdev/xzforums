define([
    'lodash',
    'marionette',

    'controllers/loginRegister',
    'delegates/login'
], function (_, Marionette, loginRegisterController, Delegator) {
    'use strict';

    var Login = Marionette.ItemView.extend({

        template: '',

        initialize: function () {
            console.log('initialize Login.');

            this.addController(loginRegisterController);
        },

        onClose: function () {

        },

        attachToView: function () {
            this.$el = $('#content form');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initView();

            this.triggerMethod('render');
        },

        onRender: function () {
            console.log('Login rendered.');
        },


        submit: function (evt) {
            var self = this;

            evt.preventDefault();

            this.disableSubmit();

            var identity = this.ui.identity.getVal(),
                password = this.ui.password.getVal();

            Delegator.login(identity, password, {
                    success: function () {
                        self.forward();
                    },
                    error: function (jqXHR, textStatus) {
                        var result;

                        if (textStatus[0] && textStatus[0].status == 403) {
                            result = JSON.parse(textStatus[0].responseText);
                            self.isServerErr = true;
                            self.ui.identity.setFail(result.message);
                            self.ui.password.setFail(result.message);
                        }
                    }
            });
        },

        forward: function () {
            var nextReg = /[\?&]?next=([^&]+)/ig,
                result;

            if (location.search.trim()) {
                result = nextReg.exec(location.search.trim());
            }

            if (result && result[1]) {
                window.location = unescape(result[1]);

                return true;
            }

            window.location = '/';
        }
    });

    return Login;
});