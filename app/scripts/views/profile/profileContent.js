define([
    'lodash',
    'marionette',

    'vent'
], function (_, Marionette, vent) {

    var Content = Marionette.ItemView.extend({

        ui: {
            img: '.avatar-holder img'
        },

        ops: {
        },

        initialize: function () {
            console.log('initialize ProfileContent');
        },

        onClose: function () {
            this.$el.find('.avatar-holder img').off('click');
        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initView();
        },

        initView: function () {
            var img = this.$el.find('.avatar-holder img');
            if (this.model.getId())
                img.click(_.bind(this.showPreview, this));
            else
                img.css('cursor', 'auto');
        },

        showPreview: function () {
                vent.trigger('profile:upload:avatar');
        }
    });

    return Content;
});
