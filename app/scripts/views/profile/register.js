define([
    'marionette',

    'utils/util',

    'delegates/register',

    'controllers/loginRegister'
], function (Marionette, Util, Delegator, loginRegisterController) {
   'use strict';

    var Register = Marionette.ItemView.extend({

        events: {
            'blur input[name=identity]': 'identityOnBlur'
        },

        initialize: function () {
            console.log('initialize Register.');

            this.addController(loginRegisterController);
        },

        onClose: function () {
        },

        attachToView: function () {
            this.$el = $('#content form');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.triggerMethod('render');
        },

        onRender: function () {
            console.log('Register rendered.');

            this.initView();
        },

        identityOnBlur: function () {
            var self = this,
                identity = this.ui.identity, val;

            if (identity.getVal() && this.ui.identity.isValid()) {
                val = identity.getVal();

                Delegator.checkIdentity(val, function (data) {
                    var field;

                    if (data && data.exist_result && data.exist_result.exist) {
                        if (self.registerType == 'U')
                            field = 'Username';
                        else
                            field = 'Email';

                        self.ui.identity.setFail('Sorry, ' + field + ' is taken.');
                        self.disableSubmit();
                    }
                });
            }
        },

        submit: function (evt) {
            var self = this;

            evt.preventDefault();

            this.disableSubmit();

            var identity = this.ui.identity.getVal(),
                password = this.ui.password.getVal();

            Delegator.register(identity, password, {
                success: function () {
                    self.forward();
                },
                error: function (jqXHR, textStatus) {
                    var result;

                    if (textStatus[0]) {
                        if (textStatus[0].status == 403) {
                            result = JSON.parse(textStatus[0].responseText);
                            self.ui.identity.setFail(result.message);
                            self.ui.password.setFail(result.message);
                        } else if (textStatus[0].status == 409) {
                            self.ui.identity.setFail('Email taken, try logging in or a different email address.');
                        }
                    }
                }
            });
        },

        forward: function () {
            var nextReg = /[\?&]?next=([^&]+)/ig,
                result;

            if (location.search.trim()) {
                result = nextReg.exec(location.search.trim());
            }

            if (this.registerType == 'E') {
                if (result && result[1]) {
                    window.location = '/register/displayName/?next=' + result[1];
                }

                window.location = '/register/displayName/';
                return  true;
            }

            if (result && result[1]) {
                window.location = unescape(result[1]);

                return true;
            }

            window.location = '/';
        }
    });

    return Register;
});