define([
    'lodash',
    'marionette',

    'vent',

    'controllers/uploadImage'
], function (_, Marionette, vent, uploadImageController) {
    'use strict';

    var ProfileMenu = Marionette.ItemView.extend({

        ui: {
            upload: '.upload'
        },

        events: {
            'click .upload': 'showPreview'
        },

        initialize: function () {
            console.log('initialize ProfileMenu.');

            this.addController(uploadImageController);

            vent.on('profile:upload:avatar', _.bind(this.showPreview, this));
        },

        onClose: function () {

        },

        attachToView: function () {
            this.$el = $('#profile-menu');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initModel();
        },

        initModel: function () {
            var id = this.$el.attr('data-id');
            this.model.setId(id);
        }
    });

    return ProfileMenu;
});
