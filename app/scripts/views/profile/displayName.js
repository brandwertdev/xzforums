define([
    'marionette',

    'delegates/register',

    'views/util/textInput'
], function (Marionette, Delegator, TextInput) {
    'use strict';

    var View = Marionette.ItemView.extend({
        ui: {
            submit: 'input[type=submit]'
        },

        events: {
            'blur input[name=displayName]': 'inputOnBlur',
            'click input[type=submit]': 'submit'
        },

        initialize: function () {
            console.log('initialize DisplayName.');
        },

        onClose: function () {
        },

        attachToView: function () {
            this.$el = $('#displayNameForm');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.initView();
        },

        onRender: function () {
        },

        initView: function () {
            this.ui.displayName = new TextInput({
                el: this.$el.find('input[name=displayName]'),
                validators: this.getValidators(),
                inputCallback: _.bind(this.checkSubmit, this)
            });

            this.ui.displayName.initView();
        },

        getValidators: function () {
            return [{
                validate: function (val) {
                    return val.length > 0 && val.length < 254;
                },
                msg: 'Display name should be more than 0, less than 254 characters.'
            }];
        },

        inputOnBlur: function () {
            var self = this,
                displayName = this.ui.displayName, val;

            if (displayName.getVal() && displayName.isValid()) {
                val = displayName.getVal();

                Delegator.checkDisplayName(val, function (data) {
                    if (data && data.exist_result && data.exist_result.exist) {
                        self.ui.displayName.setFail('Someone took this name, sorry, please try a different one.');
                        self.disableSubmit();
                    }
                });
            }
        },

        checkSubmit: function () {
            if (this.ui.displayName.isValid())
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        disableSubmit: function () {
            this.ui.submit.parent().removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submit.parent().addClass('active');
        },

        submit: function (evt) {
            var self = this;

            evt.preventDefault();

            this.disableSubmit();

            var displayName = this.ui.displayName.getVal();

            Delegator.updateDisplayName(displayName, {
                success: function () {
                    self.forward();
                },
                error: function (jqXHR, textStatus) {
                    if (textStatus[0]) {
                        if (textStatus[0].status == 409) {
                            self.ui.displayName.setFail('Someone took this name, sorry, please try a different one.');
                            self.ui.submit.parent().removeClass('active');
                        }
                    }
                }
            });
        },

        forward: function () {
            var nextReg = /[\?&]?next=([^&]+)/ig,
                result;

            if (location.search.trim()) {
                result = nextReg.exec(location.search.trim());
            }

            if (result && result[1]) {
                window.location = '/register/avatar/?next=' + result[1];
            }

            window.location = '/register/avatar/';
            return  true;
        }
    });

    return View;
});