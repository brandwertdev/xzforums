define([
    'lodash',
    'marionette',
    'vent',

    'utils/util',

    'controllers/vote',

    'collections/commentList',
    'models/comment',
    'delegates/comment'
], function (_, Marionette, vent, Util, VoteController, CommentList, Comment, Delegator) {
    var CommentComposite = Marionette.CompositeView.extend({
        popTpl:
            '<div data-role="popup" class="comment-pop"  data-theme="b">' +
                '<div data-role="controlgroup" data-type="horizontal">' +
                    '<a href="#" class="add" data-role="button" data-mini="true" data-icon="plus">Reply</a>' +
                    '<a href="#" class="edit" data-role="button" data-mini="true" data-icon="edit">Edit</a>' +
                    '<a href="#" class="delete" data-role="button" data-mini="true" data-icon="delete">Delete</a>' +
                '</div>' +
            '</div>',

        ops: {
            isItem: false
        },

        ui: {
            body: '.comment-body'
        },

        events: {
            'click .comment-body': 'clickBody'
        },

        modelEvents: {
            'destroy': 'destory'
        },

        initialize: function () {
            console.log('initialize CommentItem.');

            this.ops.isItem = this.$el.is('li');
            if (this.ops.isItem) {
                this.delegator = Delegator;
                this.addController(VoteController);

                this.permission = this.options.permission;
                delete this.options.permission;
            }
        },

        onClose: function () {

        },

        destory: function () {
            this.ui.status.html('deleted 0 second ago');

            vent.trigger('viewPost:comment:delete');
        },

        delegateEvents: function () {
            if (this.ops.isItem)
                Marionette.View.prototype.delegateEvents.call(this);
        },

        bindUIElements: function () {
            if (this.ops.isItem)
                Marionette.View.prototype.bindUIElements.call(this);
        },

        attachToView: function () {
            var self = this;

            this.bindUIElements();
            this.delegateEvents();

            if (!this.ops.isItem) {
                this.$el.children().each(function () {
                    var commentEl = $(this),
                        commentList = new CommentList(),
                        comment = new Comment(), commentItem;

                    commentItem = new CommentComposite({
                        model: comment,
                        collection: commentList,
                        el: commentEl,
                        permission: self.options.permission
                    });

                    commentItem.attachToView();
                });
            } else {
                this.initModel();
                this.initPopup();
            }
        },

        initModel: function () {
            var el = this.$el,
                id = el.attr('data-id'),
                forumSlug = el.attr('data-forum-slug'),
                postId = el.attr('data-post-id'),
                postSlug = el.attr('data-post-slug'),
                isActive = this.ui.body.text().trim() ? true : false,
                points = parseInt(this.ui.points.text()),
                myVote_num = parseInt(this.$el.attr('data-myvote'));

            this.model.set({
                entity_id: id,
                active: isActive,
                forum_slug: forumSlug,
                post_id: postId,
                post_slug: postSlug,
                points: points,
                my_vote_number: myVote_num
            }, {
                silent: true
            });
        },

        initPopup: function () {
            var model = this.model, url;

            if (model.isActive()) {
                this.$el.append((this.actionPop = $(this.popTpl)));

                this.actionPop.popup({positionTo: this.$el}).trigger('create');

                url = '/' + [model.getForumSlug(), model.getPostId(), model.getPostSlug(), 'comment', model.getId()].join('/');

                this.actionPop.find('.add').attr('href', url + '/add_comment/');
                this.actionPop.find('.edit').attr('href', url + '/edit/');
                this.actionPop.find('.delete').click(_.bind(this.clickDelete, this));
            }
        },

        clickBody: function () {
            if (this.model.isActive()) {
                if(!this.permission.get('canCreateComment')) {
                    this.actionPop.find('.add').hide()
                } else {
                    this.actionPop.find('.add').show()
                }

                this.actionPop.popup({positionTo: this.$el}).trigger('create');

                this.actionPop.popup('open');
            }
        },

        clickDelete: function (evt) {
            var self = this;

            evt.preventDefault();

            this.actionPop.popup('close');

            Util.dialog({
                header: 'Delete Comment',
                content: 'Are you sure you want to delete this comment?',
                button: {
                    text: 'Delete',
                    callback: function () {
                        var model = self.model,
                            url = '/' +
                                [model.getForumSlug(), model.getPostId(), model.getPostSlug(), 'comment', model.getId()].join('/') +
                                '/delete/';

                        window.location = url;
                    }
                }
            });
        }

    });

    return CommentComposite;
});
