define([
    'marionette'
], function (Marionette) {

    var ContentView = Marionette.ItemView.extend({

        ui: {
            body: 'textarea[name=body]',
            submit: 'input[type=submit]'
        },

        events: {
            'input textarea[name=body]': 'inputBody',
            'click .cancel':'goBack',
            'click [type=submit]': 'clickSubmit'
        },

        ops: {
            validFields: {
                body: false
            }
        },

        initialize: function () {
            console.log('initialize CreateEditCommentContent.');
        },

        onClose: function () {

        },

        attachToView: function () {
            this.$el = $('#content');
            this.el = this.$el[0];

            this.bindUIElements();
            this.delegateEvents();

            this.triggerMethod('render');
        },

        onRender: function () {
            if (this.ui.body.val())
                this.inputBody();
        },

        inputBody: function () {
            var body = this.ui.body, val = body.val();

            if (val.length > 0 && val.length <= 63206)
                this.ops.validFields.body = true;
            else
                this.ops.validFields.body = false;

            this.checkSubmit();
        },

        checkSubmit: function () {
            if (this.ops.validFields.body)
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        disableSubmit: function () {
            this.ui.submit.parent().removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submit.parent().addClass('active');
        },

        goBack: function () {
            history.back();
        },

        clickSubmit: function () {
            this.disableSubmit();
        }
    });

    return ContentView;
});
