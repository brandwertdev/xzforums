define([
    'lodash',
    'marionette',

    'vent'
], function(_, marionette, vent) {
    'use strict';

    return marionette.AppRouter.extend({
        appRoutes:{
            'home(/)': 'setHome',
            'login(/)': 'setLogin'
        },

        initialize: function () {
            vent.on('ajax:401', _.bind(this.unauthHandler, this));
            vent.on('router:navigate', _.bind(this.navigateTo, this));
        },

        unauthHandler: function () {
            this.navigate('login/', { trigger: true });
        },

        navigateTo: function (fragment) {
            this.navigate(fragment, { trigger: true });
        }
    });

});