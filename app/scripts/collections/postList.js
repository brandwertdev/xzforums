define([
    'backbone',

    'models/post',

    'utils/util'
], function (Backbone, Post, Util) {
    'use strict';

    var PostList = Backbone.Collection.extend({

        model: Post,

        url: function() {
            return Util.getApiUrl('posts');
        },

        parse: function(response) {
            this.forum = response.forum;
            this.page = response.page;

            return response.posts;
        }
    });

    return PostList;
});