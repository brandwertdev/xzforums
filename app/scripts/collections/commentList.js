define([
    'backbone',

    'models/comment',

    'utils/util'
], function (Backbone, Comment, Util) {
    'use strict';

    var CommentList = Backbone.Collection.extend({

        model: Comment,

        url: function() {
            return Util.getApiUrl('');
        }
    });

    return CommentList;
});