define([
    'backbone',

    'models/forum',

    'utils/util'
], function (Backbone, Forum, Util) {
    'use strict';

    var ForumList = Backbone.Collection.extend({

        model: Forum
    });

    return ForumList;
});