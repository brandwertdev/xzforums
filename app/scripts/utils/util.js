define([
    'lodash'
],

function(_) {
    var util = {

        dialogTpl:
            '<div data-role="popup">' +
                '<div data-role="header" data-theme="a" class="ui-corner-top">' +
                    '<h1></h1>' +
                '</div>' +
                '<div data-role="content" class="ui-corner-bottom ui-content">' +
                    '<div class="content">' +
                    '</div>' +
                    '<div class="buttons text-right">' +
                        '<a href="#" data-role="button" data-inline="true" data-rel="back" data-theme="c">Cancel</a>' +
                        '<a data-role="button" class="confirm" data-inline="true" data-theme="b"></a>' +
                    '</div>' +
                '</div>' +
            '</div>',

        popTpl:
            '<div data-role="popup" data-theme="e">' +
            '</div>',

        menuTpl:
            '<div data-role="popup">' +
            '</div>',

        getApiUrl: function(path) {
            return '/api/' + path;
        },

        slugFormat: function (title) {
            var temp = title;
            if (temp.length > 50)
                temp = temp.substr(0, 50);

            return encodeURI(temp.replace(/\s+/g, '_'));
        },

        getUrlPath: function () {
            return document.location.pathname;
        },

        loadingShow: function () {
            $.mobile.loading('show', {
                textVisible: true,
                text: 'loading...',
                theme: 'd'
            });
        },

        loadingHide: function () {
            $.mobile.loading('hide');
        },

        dialog: function (options) {
            var self = this;

            options = options || {};

            if (!options.header)
                options.header = 'Dialog';

            if (!options.content)
                options.content = 'content';

            if (!options.button)
                options.button.text = 'confirm';

            if (!this._dialog) {
                this._dialog = $(this.dialogTpl);
                $.mobile.activePage.append(this._dialog);

                $.mobile.activePage.page('destroy').page();
            }
            this._dialog
                .find('[data-role=header] h1')
                .html(options.header)
                .end()
                .find('.content')
                .html('<p>' + options.content + '</p>')
                .end()
                .find('.confirm .ui-btn-text')
                .html(options.button.text);

            this._dialog.find('.confirm').off('click');

            if (options.button.callback) {
                this._dialog.find('.confirm').click(function () { options.button.callback(self._dialog) });
            }

            this._dialog.popup('open', { positionTo: 'window'});
        },

        popup: function (elem) {
            if (!this._popupDialog) {
                this._popupDialog = $(this.popTpl);
                $.mobile.activePage.append(this._popupDialog);

                $.mobile.activePage.page('destroy').page();
            }

            this._popupDialog.html(elem);

            this._popupDialog.trigger('create');

            this._popupDialog.popup('open', { positionTo: 'window'});
        },

        showMenu: function (elem) {
            if (!this._menuDialog) {
                this._menuDialog = $(this.menuTpl);
                $.mobile.activePage.append(this._menuDialog);

                $.mobile.activePage.page('destroy').page();
            }

            this._menuDialog.html(elem);

            this._menuDialog.trigger('create');

            this._menuDialog.popup('open', { positionTo: 'window'});
        },

        crop: function (imgSrc, options) {
            var img = new Image();
            img.onload = function () {
            var canvas = document.createElement('canvas'),
                ctx = canvas.getContext('2d'),
                x = parseInt(options.x1),
                y = parseInt(options.y1),
                w = parseInt(options.x2 - x),
                h = parseInt(options.y2 - y);

                canvas.width = w;
                canvas.height = h;

                ctx.drawImage(img, x, y, w, h, 0, 0, w, h);

                options.callback(canvas.toDataURL());
            }

            img.src = imgSrc;
        }
    };

    return util;
});
