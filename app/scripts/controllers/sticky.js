define([
    'utils/util'
], function (Util) {
    'use strict';

    return {
        events: {
            'click .action .sticky': 'toggleSticky'
        },

        ui: {
            stickyIcon: '.sticky i'
        },

        modelEvents: {
            'change:sticky': 'toggleStickyIcon'
        },

        toggleSticky: function () {
            var self = this,
                post = this.model, content, text;

            if (post.isSticky()) {
                content = 'Unstick the topic from top?';
                text = 'Yes, unstick it';
            } else {
                content = 'Stick the topic on top?';
                text = 'Yes, stick it';
            }

            Util.dialog({
                header: 'Stick Post',
                content: content,
                button: {
                    text: text,
                    callback: function (dialog) {
                        self.delegator.sticky(post.getId(), function () {
                            post.setSticky(!post.isSticky());
                        });

                        dialog.popup('close');
                    }
                }
            });
        },

        toggleStickyIcon: function () {
            var isSticky = this.model.isSticky();

            if (isSticky) {
                this.ui.stickyIcon.removeClass('icon-sort').addClass('icon-pushpin');
            } else {
                this.ui.stickyIcon.removeClass('icon-pushpin').addClass('icon-sort');
            }
        }
    };
});
