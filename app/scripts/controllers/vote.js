define([
], function () {
    'use strict';

    return {
        events: {
            "click .vote .up a"       : "voteUp",
            "click .vote .down a"       : "voteDown"
        },

        ui: {
            voteUpAnchor: '.vote .up a',
            voteDownAnchor: '.vote .down a',
            points: '.vote .display'
        },

        modelEvents: {
            'change:points': 'changePoints',
            'change:my_vote_number': 'changeMyVote'
        },

        voteUp: function (evt) {
            evt.preventDefault();

            this.vote('up');
        },

        voteDown: function (evt) {
            evt.preventDefault();

            this.vote('down');
        },

        vote: function (action) {
            var myVote, points, myNewVote, id, flag,
                self = this;

            myVote = this.model.getMyVote();
            points = this.model.getPoints();

            if (action == 'up')
                flag = 1;
            else if (action == 'down')
                flag = -1;

            if(myVote == flag) {
                points = points - flag;
                myNewVote = 0;
            } else if (myVote == (-flag)) {
                points = points + (flag > 0 ? 2 : -2);
                myNewVote = flag;
            } else {
                points = points + flag;
                myNewVote = flag;
            }

            id = this.model.getId();
            this.delegator.vote(id, myNewVote, function () {
                self.model.setPoints(points).setMyVote(myNewVote);
            });
        },

        changePoints: function (model) {
            this.ui.points.text(model.getPoints());
        },

        changeMyVote: function (model) {
            var myVote = model.getMyVote(),
                voteUpAnchor = this.ui.voteUpAnchor,
                voteDownAnchor = this.ui.voteDownAnchor,
                voteUpUrl = voteUpAnchor.attr('href'),
                voteDownUrl = voteDownAnchor.attr('href');

            if (myVote == 1) {
                if (!voteUpAnchor.hasClass('active'))
                    voteUpAnchor.addClass('active');
                voteDownAnchor.removeClass('active');

                voteUpAnchor.attr('href', voteUpUrl.replace(/vote=\d/i, 'vote=0'));
                voteDownAnchor.attr('href', voteDownUrl.replace(/vote=-?\d/i, 'vote=-1'));
            } else if (myVote == -1) {
                if (!voteDownAnchor.hasClass('active'))
                    voteDownAnchor.addClass('active');
                voteUpAnchor.removeClass('active');

                voteUpAnchor.attr('href', voteUpUrl.replace(/vote=\d/i, 'vote=1'));
                voteDownAnchor.attr('href', voteDownUrl.replace(/vote=-?\d/i, 'vote=0'));
            } else {
                voteDownAnchor.removeClass('active');
                voteUpAnchor.removeClass('active');

                voteUpAnchor.attr('href', voteUpUrl.replace(/vote=\d/i, 'vote=1'));
                voteDownAnchor.attr('href', voteDownUrl.replace(/vote=-?\d/i, 'vote=-1'));
            }
        }
    };
});
