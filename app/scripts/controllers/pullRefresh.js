define([
    'lodash',

    'iscroll'
], function (_){
    'use strict';

    var PullRefresh = function (options) {
        this.wrapper = _.isString(options.wrapper) ? $(options.wrapper) : options.wrapper;
        delete options.wrapper;


        _.extend(this, {
            readyLoad: false,
            loading: false,
            loadEnd: false
        },options);

        this.init();
    };

    _.extend(PullRefresh.prototype, {
        init: function () {

            var wrapper = $(this.wrapper),
                refreshIcon = this.refreshIcon = wrapper.find('.pull-refresh-icon'),
                self = this;

            this.iscroll = new iScroll(this.wrapper.length ? this.wrapper[0] : this.wrapper, {
                useTransition: true,
                onRefresh: function () {
                    console.log('pull refresh');
                    if(this.wrapperH < this.scrollerH && !self.loadEnd) {
                        refreshIcon.show();
                    } else {
                        refreshIcon.hide();
                    }
                },
                onScrollMove: function () {
                    if (!self.loadEnd && !self.loading && this.y < (this.maxScrollY - 50) && !refreshIcon.hasClass('flip')) {
                        refreshIcon.addClass('flip').removeClass('loading');
                        refreshIcon.find('.label').text('Release to refresh...');
                        this.maxScrollY = this.maxScrollY - 60;
                        self.readyLoad = true;
                    }
                },
                onScrollEnd: function () {
                    if (!self.loadEnd && refreshIcon.hasClass('flip') && self.readyLoad) {
                        var iscroll = this;
                        self.readyLoad = false;

                        refreshIcon.removeClass('flip').addClass('loading');
                        refreshIcon.find('.label').text('Loading...');
                        self.loading = true;

                        if (self.load)
                            $.Deferred(self.load)
                                .always(function () {
                                    iscroll.maxScrollY = iscroll.maxScrollY + 60;
                                    iscroll._resetPos(400);
                                    self.loading = false;
                                    self.resetRefreshIcon();

                                    _.delay(_.bind(iscroll.refresh, iscroll), 20);
                                })
                                .done(function() {

                                })
                                .fail(function () {
                                });
                    }
                }
            });
        },

        resetRefreshIcon: function () {
            this.refreshIcon.removeClass('flip loading');
            this.refreshIcon.find('.label').text('Pull up to load more...');
        },

        refresh: function () {
            this.iscroll.refresh();
        },

        scrollTo: function (resetX, resetY, time) {
            this.iscroll.scrollTo(resetX, resetY, time || 0)
        },

        setLoadEnd: function (isEnd) {
            this.loadEnd = isEnd;
        }
    });

    return PullRefresh;
});