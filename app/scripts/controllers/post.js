define([
    'delegates/post'
], function (Delegator) {
    'use strict';

    return {

        ui: {
            commentCount: '.comment .comments_count'
        },

        modelEvents: {
            'change:comments_count': 'changeCommentCount'
        },

        initModel: function () {
            var points = parseInt(this.ui.points.text()),
                postId = this.$el.attr('data-id'),
                myVote_num = parseInt(this.$el.attr('data-myvote')),
                comment_num = parseInt(this.ui.commentCount.text()),
                follower_num = parseInt(this.ui.followCount.text()),
                isFollowed = this.$el.attr('data-isfollowed')=='True',
                isLock = this.$el.find('.action .lock').attr('data-lock') == '1',
                isSticky = this.$el.find('.action .sticky').attr('data-sticky') == '1';

            this.model.set({
                'entity_id': postId,
                'points': points,
                'my_vote_number': myVote_num,
                'follower_count': follower_num,
                'is_followed': isFollowed,
                'comments_count': comment_num,
                'lock': isLock,
                'sticky': isSticky
            }, {
                silent: true
            });
        },

        changeCommentCount: function (model) {
            this.ui.commentCount.text(model.getCommentsCount());
        },

        setRenderHelpers: function () {
            var helpers = {


                getCurrentPage: function () {
                    return _.result(this, 'belongPage');
                },

                voteUpActive: function () {
                    return this.model.getMyVote() == 1 ? 'active' : '';
                },

                voteDownActive: function () {
                    return this.model.getMyVote() == -1 ? 'active' : '';
                }
            };

            return {
                belongPage: _.bind(helpers.getCurrentPage, this),
                voteUpActive: _.bind(helpers.voteUpActive, this),
                voteDownActive: _.bind(helpers.voteDownActive, this)
            };
        }
    };
});