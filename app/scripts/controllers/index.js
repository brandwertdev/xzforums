define([
    'lodash',
    'backbone',
    'marionette',

    'vent',

    'views/util/mainLayout',
    'views/forum/postList',
    'views/forum/postListMenu',
    'views/profile/login',
    'views/profile/register',
    'views/profile/displayName',
    'views/profile/pickAvatar',
    'views/post/viewPostContent',
    'views/post/viewPostMenu',
    'views/post/createEditPostContent',
    'views/comment/createEditCommentContent',
    'views/profile/profileContent',
    'views/profile/profileMenu',
    'views/admin/manageForums',
    'views/admin/createForum',
    'views/admin/editPermission',
    'views/admin/manageGroups',

    'collections/postList',
    'models/post',
    'models/comment',
    'models/user',
    'models/permission'
], function (_, Backbone, Marionette, vent, MainLayout,  PostListView, PostListMenuView, LoginView,
             RegisterView, DisplayNameView, PickAvatarView, ViewPostContent, ViewPostMenu, CreateEditPostContent, CreateEditCommentContent,
             ProfileContent, ProfileMenu, ManageForumsView, CreateForumView, EditPermissionView, ManageGroupsView, PostListCollection,
             Post, Comment, User, Permission) {
    'use strict';

    var Controller = Marionette.Controller.extend({

        initialize: function () {

        },

        setHome: function () {
            console.log('this is setHome');
        },

        setLogin: function () {
            var loginView = new LoginView();

            this.app.content.show(loginView);
        },

        initForum: function () {
            var postListView = new PostListView({ collection: new PostListCollection() });

            this.useMainLayout({
                mainContent: {
                    view: postListView
                },
                contentMenu: {
                    view: PostListMenuView,
                    options: {
                        ops: postListView.ops
                    }
                }
            });
        },

        initLogin: function () {
            var loginView = new LoginView();

            this.app.content.attachView(loginView);
            loginView.attachToView();
        },

        initRegister: function () {
            var registerView = new RegisterView();

            this.app.content.attachView(registerView);
            registerView.attachToView();
        },

        initDisplayName: function () {
            var displayNameView = new DisplayNameView();

            this.app.content.attachView(displayNameView);
            displayNameView.attachToView();
        },

        initPickAvatar: function () {
            var pickAvatarView = new PickAvatarView({
                model: new User()
            });

            this.app.content.attachView(pickAvatarView);
            pickAvatarView.attachToView();
        },

        initViewPost: function () {
            var post = new Post(),
                permission = new Permission();

            this.useMainLayout({
                mainContent: {
                    view: ViewPostContent,
                    options: {
                        model: post,
                        permission: permission
                    }
                },
                contentMenu: {
                    view: ViewPostMenu,
                    options: {
                        model: post,
                        permission: permission
                    }
                }
            });
        },

        initCreateEditPost: function () {
            this.useMainLayout({
                mainContent: {
                    view: CreateEditPostContent,
                    options: {
                        model: new Post()
                    }
                }
            });
        },

        initCreateEditComment: function () {
            this.useMainLayout({
                mainContent: {
                    view: CreateEditCommentContent,
                    options: {
                        model: new Comment()
                    }
                }
            });
        },

        initProfile: function () {
            var user = new User();

            this.useMainLayout({
                contentMenu: {
                    view: ProfileMenu,
                    options: {
                        model: user
                    }
                },
                mainContent: {
                    view: ProfileContent,
                    options: {
                        model: user
                    }
                }
            });
        },

        initManageForums: function () {
            this.useMainLayout({
                mainContent: {
                    view: ManageForumsView
                }
            });
        },

        initCreateForum: function () {
            this.useMainLayout({
                mainContent: {
                    view: CreateForumView
                }
            });
        },

        initEditPermission: function () {
            this.useMainLayout({
                mainContent: {
                    view: EditPermissionView
                }
            });
        },

        initManageGroups: function () {
            this.useMainLayout({
                mainContent: {
                    view: ManageGroupsView
                }
            });
        },

        useMainLayout: function (options) {
            var mainLayout = new MainLayout();

            this.app.content.attachView(mainLayout);

            _.each(options, function (value, key) {
                var view;

                if (_.isFunction(value))
                    view = new value();
                else if (_.isObject(value)) {
                    if (_.isFunction(value['view']))
                        view = new value['view'](value['options'] ? value['options'] : {});
                    else if (_.isObject(value['view']))
                        view = value['view'];
                }

                mainLayout[key].attachView(view);

                view.attachToView();
            });
        },

        initContentView: function () {
            var initMap = {
                ':forumSlug(/)': 'initForum',
                ':forumSlug/:postId(/)(:postSlug)(/)': 'initViewPost',
                ':forumSlug/topic/create(/)': 'initCreateEditPost',
                ':forumSlug/:postId/edit(/)(:postSlug)(/)': 'initCreateEditPost',
                ':forumSlug/:postId/:postSlug/add_comment(/)': 'initCreateEditComment',
                ':forumSlug/:postId/:postSlug/comment/:commentId/edit(/)': 'initCreateEditComment',
                ':forumSlug/:postId/:postSlug/comment/:commentId/add_comment(/)': 'initCreateEditComment',
                'admin/(:display_name/)manage_forums(/)':'initManageForums',
                'admin/(:display_name/)manage_groups(/)':'initManageGroups',
                'admin/create_forum(/)':'initCreateForum',
                'admin(/:display_name)/edit_permission/(:entity)/(:entity_id)/role/(:role_id)/':'initEditPermission',
                'user(/)(:displayName)(/)': 'initProfile',
                'login(/)': 'initLogin',
                'register(/)': 'initRegister',
                'register/displayName(/)':'initDisplayName',
                'register/avatar(/)':'initPickAvatar',
                '(/)': 'initForum'
            }
            , route
            , routes = _.keys(initMap)
            , fragment
            , matchedHandler, args;

            this.initRouter = [];

            while ((route = routes.pop()) != null) {
                this._routeInit(route, initMap[route]);
            }

            fragment = this.fragment = Backbone.history.getFragment();

            matchedHandler = _.find(this.initRouter, function(handler) {
                return handler.route.test(fragment);
            });

            if (matchedHandler) {
                args = this._extractParameters(matchedHandler.route, fragment);
                matchedHandler.callback && matchedHandler.callback.apply(this, args);
            }
        },

        _routeInit: function (route, name, callback) {
            if (!_.isRegExp(route)) route = this._routeToRegExp(route);
            if (_.isFunction(name)) {
                callback = name;
                name = '';
            }
            if (!callback) callback = this[name];

            this.initRouter.push({
                route: route,
                callback: callback
            });

            return this;
        },

        _routeToRegExp: function(route) {
            var optionalParam = /\((.*?)\)/g;
            var namedParam    = /(\(\?)?:\w+/g;
            var splatParam    = /\*\w+/g;
            var escapeRegExp  = /[\-{}\[\]+?.,\\\^$|#\s]/g;

            route = route.replace(escapeRegExp, '\\$&')
                .replace(optionalParam, '(?:$1)?')
                .replace(namedParam, function(match, optional){
                    return optional ? match : '([^\/]+)';
                })
                .replace(splatParam, '(.*?)');
            return new RegExp('^' + route + '$');
        },

        _extractParameters: function(route, fragment) {
            var params = route.exec(fragment).slice(1);
            return _.map(params, function(param) {
                return param ? decodeURIComponent(param) : null;
            });
        }
    });

    return Controller;
});