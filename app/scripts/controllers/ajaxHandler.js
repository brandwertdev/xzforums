define([
    'vent',
    'utils/util'
], function (vent, Util) {
    'use strict';

    $(document).ajaxError(function(event, request, settings) {
        if (request && request.status == 401)
            window.location = '/login/';
        else
            Util.popup('<p>' + request.statusText + '</p>');
    });

    $(document).ajaxStart(function() {
        Util.loadingShow();
    });

    $(document).ajaxComplete(function() {
        Util.loadingHide();
    });
});