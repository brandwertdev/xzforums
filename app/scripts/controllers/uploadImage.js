define([
    'lodash',

    'utils/util',
    'delegates/signature',

    'jcrop',
    'canvasResize',
    'transloadit'
], function (_, Util, Delegator) {
    'use strict';

    return {
        previewTpl:
            '<div data-role="popup" class="preview-avatar-pop" data-corners="false">' +
                '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>' +
                '<div class="image-holder">' +
                    '<img draggable="false">' +
                '</div>' +

                '<form class="upload-image-form" action="http://api2.transloadit.com/assemblies" enctype="multipart/form-data" method="POST">' +
                    '<input type="hidden" name="params" value=""/>' +
                    '<input type="hidden" name="signature" value="">' +
                    '<input type="hidden" name="user_id" value="">' +
                    '<input type="hidden" name="width" value="">' +
                    '<input type="hidden" name="height" value="">' +
                    '<input type="hidden" name="x" value="">' +
                    '<input type="hidden" name="y" value="">' +
                    '<input type="hidden" name="x2" value="">' +
                    '<input type="hidden" name="y2" value="">' +
                    '<input type="file" id="upload-avatar" title="upload image" accept="image/*" name="my_file" />' +
                    '<input type="submit" value="upload">' +
                '</form>' +
            '</div>',

        events: {
        },

        ui: {
        },

        modelEvents: {
        },

        showPreview: function () {
            var pop, self = this;

            if (!this.ui.previewPop) {
                pop = this.ui.previewPop = $(this.previewTpl);
                $.mobile.activePage.append(pop);
                $.mobile.activePage.page('destroy').page();

                Delegator.getSignature(function (data) {
                    pop.find('[name=params]')
                        .val(data.auth.params)
                        .end()
                        .find('[name=signature]')
                        .val(data.auth.signature)
                        .end()
                        .find('[name=user_id]').val(self.model.getId());
                });

                this.ui.previewImg = pop.find('img');
                this.ui.file = pop.find('input:file');
                this.ui.x1 = pop.find('[name=x]');
                this.ui.y1 = pop.find('[name=y]');
                this.ui.x2 = pop.find('[name=x2]');
                this.ui.y2 = pop.find('[name=y2]');
                this.ui.form = pop.find('form');

                this.ui.file.on('change', _.bind(this.fileChanged, this));

                this.setupTransliadit();
            }

            this.ui.previewPop.popup('open', { positionTo: 'window' });
        },

        setupTransliadit: function () {
            var self = this;

            self.ui.form.transloadit({
                //wait: true,
                autoSubmit: false,
                fields: true,
                onSuccess: function () {
                    self.ui.previewPop.popup('close');

                    Util.popup('<p>Success!</p>');

                    Util.crop(self.ui.previewImg.attr('src'), {
                        x1: self.ui.x1.val(),
                        y1: self.ui.y1.val(),
                        x2: self.ui.x2.val(),
                        y2: self.ui.y2.val(),
                        callback: function (data) {
                            $('.avatar-holder img').attr('src', data)
                        }
                    });

                    self.uploadSuccess && self.uploadSuccess();
                }
            });
        },

        fileChanged: function (evt) {
            var self = this,
                file = evt.target.files[0],
                filter = /^(?:image\/(?:jpg|png|jpeg))$/i;


            if (this.jcrop_api)
                this.jcrop_api.destroy();

            if (!file) {
                self.ui.previewImg.attr('src', '').attr('style', '');
                self.ui.previewPop.popup('reposition', { positionTo: 'window' });
                return;
            }

            if (!filter.test(file.type)) {
                self.ui.previewPop.popup('close');

                Util.popup('<p>Only support .PNG or .JPG image!</p>');
                return;
            }

            if (file.size > 10000000)  {
                self.ui.previewPop.popup('close');

                Util.popup('<p>Size limitation is 10MB!</p>');
                return;
            }


            this.ui.previewPop.find('input:submit').parents('.ui-submit').addClass('active')

            $.canvasResize(file, {
                width: ($(window).width() - 30),
                height: ($(window).height() - 150),
                crop: false,
                quality: 80,
                //rotate: 90,
                callback: function(data) {
                    self.ui.previewPop.hide();
                    self.ui.previewImg.attr('style', '');
                    self.ui.previewImg.attr('src', data);

                    setTimeout( function () {
                        var imgWidth, imgHeight, min, c;

                        self.ui.previewPop.show();
                        self.ui.previewPop.popup('reposition', { positionTo: 'window' });

                        imgWidth = self.ui.previewImg.width();
                        imgHeight = self.ui.previewImg.height();

                        self.setSizeFields(imgWidth, imgHeight);

                        min = _.min([imgWidth, imgHeight]);

                        c = {"x":0,"y":0,"x2":min,"y2":min};

                        self.ui.previewImg.Jcrop({
                            aspectRatio: 1,
                            bgFade: true,
                            setSelect: [c.x,c.y,c.x2,c.y2],
                            keySupport:false,
                            maxSize: [min, min],
                            minSize: [min, min],
                            allowSelect: false,
                            allowResize: false,
                           onChange: function(c) { self.setCoordsFields(c) }
                        }, function () {
                            self.jcrop_api = this;
                        });

                    }, 500);
                }
            });
        },

        setSizeFields: function (width, height) {
            var pop = this.ui.previewPop,
                widthEl = pop.find('input:hidden[name=width]'),
                heightEl = pop.find('input:hidden[name=height]');

            widthEl.val(width);
            heightEl.val(height);
        },

        setCoordsFields: function (c) {
            var x1El = this.ui.x1,
                y1El = this.ui.y1,
                x2El = this.ui.x2,
                y2El = this.ui.y2;

            x1El.val(c.x);
            y1El.val(c.y);
            x2El.val(c.x2);
            y2El.val(c.y2);
        }
    };
});
