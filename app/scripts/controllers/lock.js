define([], function () {
    'use strict';

    return {
        events: {
            'click .action .lock': 'toggleLock'
        },

        ui: {
            lockIcon: '.lock i'
        },

        modelEvents: {
            'change:lock': 'toggleLockIcon'
        },

        toggleLock: function () {
            var self = this, post = this.model;

            this.delegator.lock(post.getId(), function (data, textStatus, XMLHttpRequest) {
                post.setLock(!post.isLock());
                self.lockCallback && self.lockCallback(data, textStatus, XMLHttpRequest);
            });
        },

        toggleLockIcon: function () {
            var isLock = this.model.isLock();

            if (isLock) {
                this.ui.lockIcon.removeClass('icon-unlock-alt').addClass('icon-lock');
            } else {
                this.ui.lockIcon.removeClass('icon-lock').addClass('icon-unlock-alt');
            }
        }
    };
});
