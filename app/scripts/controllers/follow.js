define([
    'delegates/post'
], function (Delegator) {
    'use strict';

    return {
        events: {
            "click .status .follow": "follow"
        },

        ui: {
            follow: '.follow',
            followCount: '.follow .ui-btn-text'
        },

        modelEvents: {
            'change:is_followed': 'changeFollowed',
            'change:follower_count': 'changeFollowCount'
        },

        follow: function (evt) {
            var isFollowed, followCount,
                self = this;

            evt.preventDefault();

            isFollowed = this.model.isFollowed();

            followCount = self.model.getFollowCount();

            Delegator.follow(this.model.getId(), !isFollowed, function () {
                self.model.setFollowed(!isFollowed).setFollowCount((isFollowed ? --followCount : ++followCount));
            });
        },

        changeFollowed: function (model) {
            var isFollowed = model.isFollowed(),
                url = this.ui.follow.attr('href');

            if (isFollowed) {
                this.ui.follow.addClass('ui-btn-active');
                this.ui.follow.attr('href', url.replace(/follow=\d/i, 'follow=0'));
            }
            else{
                this.ui.follow.removeClass('ui-btn-active');
                this.ui.follow.attr('href', url.replace(/follow=\d/i, 'follow=1'));
            }
        },

        changeFollowCount: function (model) {
            this.ui.followCount.text(model.getFollowCount());
        }
    };
});
