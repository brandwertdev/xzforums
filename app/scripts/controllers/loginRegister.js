define([
    'lodash',
    'views/util/textInput'
], function (_, TextInput) {
    'use strict';

    return {

        ui: {
            submit: 'input[type=submit]'
        },

        events: {
            'click input[type=submit]': 'submit',
            'click input[data-role=cancel]': 'goBack'
        },

        initView: function () {
            this.registerType = this.$el.attr('data-type');

            this.ui.identity = new TextInput({
                el: this.$el.find('input[name=identity]'),
                validators: this.getIdentityValidators(),
                inputCallback: _.bind(this.checkSubmit, this)
            });

            this.ui.password = new TextInput({
                el: this.$el.find('input[name=password]'),
                validators: this.getPasswordValidators(),
                inputCallback: _.bind(this.checkSubmit, this)
            });

            this.ui.identity.initView();
            this.ui.password.initView();
        },

        checkSubmit: function () {
            if (this.ui.identity.isValid() && this.ui.password.isValid())
                this.enableSubmit();
            else
                this.disableSubmit();
        },

        getIdentityValidators: function () {
            if (this.registerType == 'U') {
                return [{
                    validate: function (val) {
                        return val.length > 1 && val.length < 255;
                    },
                    msg: 'Username has to be more than 2, less than 254 characters.'
                }, {
                    validate: function (val) {
                        return /^[0-9a-z]*$/ig.test(val);
                    },
                    msg: 'Username contains invalid character, Username can only be english character or number.'
                }];
            } else if (this.registerType == 'E') {
                return [{
                    validate: function (val) {
                        var reg = /^[_0-9a-z]*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z]))$/ig;
                        return reg.test(val);
                    },
                    msg: 'Email contains invalid character, Email can only be english character, number or underline.'
                }];
            }
        },

        getPasswordValidators: function () {
            return [{
                validate: function (val) {
                    return val.length > 1 && val.length < 255;
                },
                msg: 'Password has to be more than 2, less than 254 characters.'
            }, {
                validate: function (val) {
                    return /^[0-9a-z]*$/ig.test(val);
                },
                msg: 'Password contains invalid character, Password can only be english character or number.'
            }];
        },

        goBack: function () {
            history.back();
        },

        disableSubmit: function () {
            this.ui.submit.parent().removeClass('active');
        },

        enableSubmit: function () {
            this.ui.submit.parent().addClass('active');
        }
    };
});
