// Set the require.js configuration for your application.
require.config({

    // Initialize the application with the main application file.
    deps: ['main'],

    paths: {
        // JavaScript folders.
        libs: '../scripts/libs',
        plugins: '../scripts/plugins',

        // Requirejs plugins.
//        requireText: './plugins/require-text',
//        tpl: './plugins/require-mustache',

        // Libraries.
        marionette: './libs/backbone.marionette',

        lodash: './libs/lodash',
        backbone: './libs/backbone',
        mustache: './libs/mustache',
        marionette_extend: './plugins/marionette-extend',
        endlessScroll: './plugins/jquery.endless-scroll',
        transloadit: './plugins/jquery.transloadit2-latest',
        jcrop: './plugins/jquery.Jcrop',
        qtip: './plugins/jquery.qtip',
        exif: './plugins/jquery.exif',
        canvasResize: './plugins/jquery.canvasResize',
        iscroll: './plugins/iscroll'
//	    slider: '../scripts/plugins/bootstrap-slider'
    },

    map: {
        '*': { 'underscore': 'lodash' }
    },

    shim: {
        // Backbone library depends on lodash and jQuery.
        backbone: {
            deps: ['lodash'],
            exports: 'Backbone'
        },

        marionette: {
          exports: 'Backbone.Marionette',
          deps: ['backbone']
        },

        canvasResize: {
            exports: 'canvasResize',
            deps: ['exif']
        },

        iscroll: {
            exports: 'iscroll'
        }

        // Backbone.LayoutManager depends on Backbone.
//        'plugins/backbone.layoutmanager': ['backbone'],

//	    'plugins/bootstrap-slider': {
//		    dep: ['jquery'],
//		    export: 'slider'
//	    }
    }

});

// Includes File Dependencies
require([], function() {
//
//    // Prevents all anchor click handling
    $.mobile.linkBindingEnabled = false;
//
//    // Disabling this will prevent jQuery Mobile from handling hash changes
    $.mobile.hashListeningEnabled = false;

    $.mobile.ajaxEnabled = false;

    $.mobile.pushStateEnabled = false;

} );
