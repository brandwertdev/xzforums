//define([
//    // Libraries.
//    "jquery",
//    // "jquery",
//    "lodash",
//    "backbone",
//    "mustache",
//
//    // Plugins.
//    "plugins/backbone.layoutmanager",
//
//],
//
//function($, _, Backbone, Mustache) {
//
//    // Provide a global location to place configuration settings and module
//    // creation.
//    var app = {
//        // The root path to run the application.
//        root: "/"
//
//    };
//
//
//
//    // Localize or create a new JavaScript Template object.
//    var JST = window.JST = window.JST || {};
//
//    // Configure LayoutManager with Backbone Boilerplate defaults.
//    Backbone.Layout.configure({
//        manage: true,
//
//        prefix: 'app/templates/',
//
//        fetch: function(path) {
//            path = path + ".mustache";
//
//            if (JST[path]) {
//                return JST[path];
//            }
//
//            var done = this.async();
//
//            $.get(app.root + path, function(contents) {
//
//                done(JST[path] = Mustache.compile(contents));
//
//            });
//        }
//    });
//
//    // Mix Backbone.Events, modules, and layout management into the app object.
//    return _.extend(app, {
//        // Get url for an api call
//        getApiUrl: function(path) {
//            //return 'http://testforum-002.local:5000/api/' + path;
//
//            var url = '/api/' + path;
//
//            return url;
//        },
//
//        // Create a custom object with a nested Views object.
//        module: function(additionalProps) {
//            return _.extend({ Views: {} }, additionalProps);
//        },
//
//        alert: {
//
//            show: function(alert, msg, type){
//                var alertClass = "alert-"
//                if(typeof(type) === 'undefined')
//                {
//                    alertClass += 'error';
//                }else{
//                    alertClass += type;
//                }
//
//                alert.addClass(alertClass);
//                alert.html(msg);
//                alert.show();
//            },
//
//            hide: function(alert){
//                alert.hide();
//            }
//
//        }
//
//    }, Backbone.Events);
//
//});

define([
    'lodash',

    'marionette',

    'routers/index',
    'controllers/index',

    'marionette_extend'
], function (_, Marionette, Router, Controller) {
    'use strict';

    var app, controller;

    app = new Marionette.Application();

    controller = new Controller();

    controller.app = app;

    new Router({
        controller: controller
    });

    _.extend(app, {
        root: '/'
    });

    app.addRegions({
        content: '#content'
    });

    app.addInitializer(function () {
        controller.initContentView();
    });

    return app;
});


