// This module is used to load modal popup.
// modal popup should extend from this module.

define([
	// Libararies.
	'jquery',

	// Application.
	'app'
],

function ($, app) {
	'use strict';

	var ModalPopup = app.module();

	ModalPopup = Backbone.View.extend({
		initModalWindow: function () {
			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();

			this.maskEl = $('#mask');

			//Set height and width to mask to fill up the whole screen
			this.maskEl.css({'width':maskWidth,'height':maskHeight});

			//transition effect
			this.maskEl.fadeIn(1000);
			this.maskEl.fadeTo("slow",0.8);

			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();

			//Set the popup window to center
			this.$el.css('top',  winH/2 - this.$el.outerHeight()/2);
			this.$el.css('left', winW/2 - this.$el.outerWidth()/2);

			//transition effect
			this.$el.fadeIn(2000);
		},

		close: function () {
			var self = this;

			self.$el.hide();
			self.maskEl.hide();

			self.remove();
		}
	});

	return ModalPopup;
});