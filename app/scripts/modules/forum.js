define([
    // Libararies.
    "backbone",

    // Application.
    "app"
],

function(Backbone, app) {
    "use strict";

    var Forum = app.module();

    Forum.Model = Backbone.Model.extend({
        idAttribute: 'entity_id',

        defaults: {
            'title': 'Title',
            'description': 'Description',
            'position': 0,
            'level': 0,
            'parent_id': null,
            'icon': null,
            'post_count': 0,
            'follow': false,
            'create_at': Date.now(),
            'updated_at': Date.now()
        }

    });

    Forum.Collection = Backbone.Collection.extend({
        model: Forum.Model,

        url: function() {
            return app.getApiUrl('forums');
            // return '/forums';
        },

        parse: function(response) {


            return response.forums;
        }
    });

    Forum.Views.Item = Backbone.View.extend({
        template: 'forum/item',

        tagName: 'li',

        serialize: function() {
            return { model: this.model.toJSON(), selected: this.options.selected };
        },

        beforeRender: function() {
            var forums = this.collection
            var nodeId = this.options.selected;

            //get all child nodes
            var subNodes = forums.where({parent_id: nodeId})

            //check if childs exist
            if(subNodes.length > 0)
            {
                //if yes insert new nodelist
                this.insertView(new Forum.Views.NodeList({

                    collection: forums,
                    nodeId: nodeId

                }))
            }

        },

        events: {
            // Albert: Change 'click' event to 'click a:first()' to fix one bug that click
            // sub link fire parent's click handler.
            'click a:first()': 'changeForum'
        },

        changeForum: function(ev) {
            app.router.go("forum", this.model.id);
        },

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
        }
    });

    Forum.Views.NodeList = Backbone.View.extend({

        tagName: 'ul',

        beforeRender: function() {

            var forums = this.collection;
            var nodeId = this.options.nodeId;
            var subNodes = forums.where({parent_id: nodeId})


            //loop over all children
            for( var i in subNodes )
            {
                var currentNode = subNodes[i];

                //insert new node view
                this.insertView(new Forum.Views.Item({
                    collection: forums,
                    model: currentNode,
                    selected: currentNode.get("entity_id")
                }));
            }

        }

    });


    Forum.Views.List = Backbone.View.extend({
        template: 'forum/list',

        beforeRender: function() {

            var forums = this.collection;
            var level0Node = forums.where({level: 0})[0];

            //insert tree - start with level 0
            if(forums.length > 0)
            {
                this.insertView('#forummenu', new Forum.Views.Item({
                    collection: forums,
                    model: level0Node,
                    selected: level0Node.id
                }));
            }

        },

        initialize: function() {
            var view = this;



            // Fetch default forums and posts collections.
            this.collection.fetch({reset: true, success: function(forums){

            }});

            this.listenTo(this.collection, {
                'reset': this.render,

                'fetch': function() {
                    console.log("fetching")
                }
            });


        }
    });

    return Forum;

});
