define([
	// Libararies.
	"lodash",
	"backbone",

	// Application.
	"app"
	], 

	function (_, Backbone, app) {
		'use strict';

		var Form = app.module();

		Form.Model = Backbone.Model.extend({
	        idAttribute: 'entity_id',

	        url: function() {
	            return app.getApiUrl('password_authentication/register');
	        },

	        default: {
	        	username: '',
	        	password: '',
	        	display_name: ''
	        },

	        setUsername: function (username) {
	        	this.set('username', username);
	        },

	        setPassword: function (password) {
	        	this.set('password', password);
	        },

	        setDisplayname: function (displayname) {
	        	this.set('display_name', displayname);
	        },

	        validate: function (attrs, options) {
	        	if(arguments.length == 0)
	        		return this.validate(this.attributes);

	        	if(attrs.username.length == 0)
	        		return 'username should not be empty';

	        	if(attrs.password.length == 0)
	        		return 'password should not be empty';
	        }
	    });

		Form.View = Backbone.View.extend({
			template: 'register',

			className: 'forum-register',

			map: {
				
			},

			initialize: function () {
				_.extend(this, {
					usernameInput: '',
					passwordInput: '',
					displaynameInput: ''
				});
			},

			afterRender: function () {
				_.extend(this, {
					usernameInput: this.$('#register-username input')[0],
					passwordInput: this.$('#register-password input')[0],
					displaynameInput: this.$('#register-displayname')[0]
				});
			},

			events: {
				'click #register-submit': 'submitRegister'
			},

			setModel: function (attrs) {
				this.model.set(attrs);
			},

			validate: function () {
				return this.model.validate();
			},

			submitRegister: function () {
				this.setModel({
					username: this.usernameInput.value, 
					password: this.passwordInput.value, 
					displayname: this.displaynameInput.value
				});

				this.validate();
			}			
		});

		return Register;
});