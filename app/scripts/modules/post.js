define([
    "lodash",
    "util",
    "app",
    "backbone",
    "endlessScroll",
    "delegates/post",
    "modules/forum"
],

    function(_, util, app, Backbone, eScroll, postDelegate, Forum) {
        "use strict";

        var Post = app.module();

        Post.Model = Backbone.Model.extend({

            url: function() {
                var urlString = "post/" + this.id;
                return app.getApiUrl(urlString);
            },


            idAttribute: 'entity_id',

            defaults: {
                'title': '',
                'body': '',
                'views': '',
                'sticky': '',
                'forum_id': 0,
                'points': 0,
                'author_id': "",
                'sticky_position': "",
                'entity_id': 1,
                'my_vote_number': 1,
                'create_at': Date.now(),
                'updated_at': Date.now()
            }

        });

        Post.Collection = Backbone.Collection.extend({
            model: Post.Model,
            forum: null,
            page: 1,

            url: function() {
                return app.getApiUrl('posts');
                // return '/posts';
            },

            parse: function(response) {
                this.forum = response.forum;
                this.page = response.page;

                return response.posts;
            }

        });

        Post.Views.PostEntry = Backbone.View.extend({

            template: 'post/postentry',

            serialize: function() {
                return { model: this.model.toJSON(), selected: this.options.postId };
            },

            events: {
                "click .vote .up"       : "voteUp",
                "click .vote .down"     : "voteDown",
                "click .action .follow"       : "follow"
            },



            voteUp: function(ev){
                this.vote(1)
            },

            voteDown: function(ev){
                this.vote(-1)
            },

            vote: function(vote) {

                this.setBtn(vote);
                Post.Vote(this, vote);

            },

            voteSuccess: function(){

                this.setBtn();

            },

            setBtn: function(vote){
                Post.SetVoteBtn(this, vote);
            },


            initialize: function() {

                var view = this;
                var postId = this.options.postId;
                var delegate = new postDelegate();
                delegate.url = app.getApiUrl('post/' + postId);
                this.model = new Post.Model();
;

                // get model if it not exists : deep linkin
                if(typeof(this.collection.get(postId)) === 'undefined')
                {
                    this.model.set({
                        entity_id: postId
                    });
                    delegate.getPostById(this.model, function(md, result){
                        view.model = new Post.Model(result.post)
                        view.collection.add(view.model);
                        view.model.on("change:my_vote_number", view.voteSuccess, view);
                        view.listenTo(view.model, 'change', view.render);
                        view.render();
                    });
                }else{
                    this.model = this.collection.get(postId);
                    this.model.on("change:my_vote_number", this.voteSuccess, this);
                    this.listenTo(this.model, 'change', this.render);

                }
            },



            afterRender: function(){
                this.setBtn();
            }


        });

        Post.Views.PostAdd = Backbone.View.extend({

            template: 'post/postadd',

            events: {
                "click .save"       : "save"
            },

            serialize: function() {
                return { collection: this.collection.toJSON(), selectedForum: _.isNumber(parseInt(this.options.selectedForum)) ? this.options.selectedForum : 0 };
            },

            initialize: function() {

            },

            save: function(){

                var post = new Post.Model();
                var delegate = new postDelegate();
                var inputTitle = this.$('#inputTitle')
                var inputMessage = this.$('#inputMessage')
                var errorElement = this.$el.find(".alert");
                var view = this;

                if(inputTitle.val() == '' || inputMessage.val() == '')
                {
                    app.alert.show(errorElement, 'All fields are required.');
                }else
                {
                    this.$('#btnPost').button('loading');

                    post.set({
                        title: inputTitle.val(),
                        body: inputMessage.val(),
                        forum_id: parseInt(this.options.selectedForum)
                    });

                    delegate.add(this.collection, post, function(){
                        app.alert.hide(errorElement);
                        app.router.go("forum", view.options.selectedForum);
                    });
                }

            }

        });

        Post.Views.PostEdit = Backbone.View.extend({

            template: 'post/postedit',

            events: {
                "click .save"       : "save"
            },

            serialize: function() {
                return { model: this.model.toJSON(), selectedPost: this.options.postId};
            },

            initialize: function() {
                var view = this;
                var postId = this.options.postId;
                var delegate = new postDelegate();
                delegate.url = app.getApiUrl('post/' + postId);
                this.model = new Post.Model();
                this.listenTo(this.model, 'change', this.render);

                //get model if not exist: deep linking
                if(typeof(this.collection.get(postId)) === 'undefined')
                {
                    this.model.set({
                        entity_id: postId
                    });
                    delegate.getPostById(this.model, function(md, result){
                        view.model = new Post.Model(result.post)
                        view.collection.add(view.model);
                        view.render();
                    });
                }else{
                    this.model = this.collection.get(postId);
                }

            },

            save: function(){
                var delegate = new postDelegate();
                var postId = this.options.postId;
                delegate.url = app.getApiUrl('post/' + postId);
                var inputTitle = this.$('#inputTitle');
                var inputMessage = this.$('#inputMessage');
                var errorElement = this.$el.find(".alert");
                var view = this;

                if(inputTitle.val() == '' || inputMessage.val() == '')
                {
                    app.alert.show(errorElement, 'All fields are required.');
                }else
                {
                    var attr = {
                        title: inputTitle.val(),
                        body: inputMessage.val()
                    };

                    this.$('#btnPost').button('loading');

                    delegate.update(this.model, attr, function(){
                        app.alert.hide(errorElement);
                        app.router.go("post", view.options.postId);
                    });
                }

            }

        });

        Post.SetVoteBtn = function(view, vote){
            var myVote = view.model.get("my_vote_number");

            if($.isNumeric(vote)){
                myVote = vote;
            }

            if(myVote == 1)
            {
                view.$el.find(".up a.link").addClass("voted-up");
                view.$el.find('.up a.link').bind('click', false);
                view.$el.find(".down a.link").removeClass("voted-down");
                view.$el.find('.down a.link').unbind('click', false);

            }else if (myVote == -1){
                view.$el.find(".down a.link").addClass("voted-down");
                view.$el.find('.down a.link').bind('click', false);
                view.$el.find(".up a.link").removeClass("voted-up");
                view.$el.find('.up a.link').unbind('click', false);
            }
        };

        Post.SetFollowBtn = function(view){

            var isFollowed = view.model.get("is_followed");
            var followCount = view.model.get("follower_count");

            if(isFollowed) {
                view.$(".follow").addClass("active")
                followCount = followCount + 1;
            }else{
                view.$(".follow").removeClass("active");
                followCount = followCount - 1;
            }

            //view.model.set({follower_count: followCount})

        };

        Post.Vote = function(post, vote){

            var postView = post;
            var myVote = post.model.get("my_vote_number");
            var myNewVote = parseInt(vote)
            var newPoints = post.model.get("points");

            if(myVote == 1)
            {
                newPoints = newPoints - 2;
            }else if(myVote == -1)
            {
                newPoints = newPoints + 2;
            }else{
                newPoints = newPoints + myNewVote;
            }


            var delegate = new postDelegate();
            var postId = post.model.get("entity_id");

            delegate.vote(postId, myNewVote, function(){
                postView.model.set({points: newPoints, my_vote_number: myNewVote})
            });
        };

        Post.Follow = function(post, newFollow){

            var postView = post;
            var delegate = new postDelegate();
            var postId = post.model.get("entity_id");
            var followCount = post.model.get("follower_count");


            delegate.follow(postId, newFollow , function(){
                postView.model.set({is_followed: newFollow})
                Post.SetFollowBtn(postView);

                if(newFollow) {

                    followCount = followCount + 1;
                }else{

                    followCount = followCount - 1;
                }

                postView.model.set({follower_count: followCount})

            });
        };


        Post.Views.Item = Backbone.View.extend({
            template: 'post/item',

            tagName: 'div',

            className: 'post-item clearfix',

            events: {
                "click .vote .up"       : "voteUp",
                "click .vote .down"     : "voteDown",
                "click .action .follow"       : "follow"
            },


            serialize: function() {
                var post = this.model.toJSON();

                _.extend(post, {
                    belongpage: this.model.collection.page,
                    isFollowedActive: this.isFollowedActive,
                    isFollowNum: this.isFollowNum,
                    voteUpNumConvert: this.voteUpNumConvert,
                    voteDownNumConvert: this.voteDownNumConvert
                });

                return { post: post, selected: this.options.selected, slugFormat: util.slugFormat};
            },

            beforeRender: function() {

            },

            afterRender: function(){
                this.setBtn();
            },

            follow: function(evt){
                evt.preventDefault();

                var isFollowed = this.model.get("is_followed");
                var newFollow = true;

                if(isFollowed) {
                    newFollow = false;
                }

                Post.Follow(this, newFollow)
            },

            setBtn: function(vote){

                this.setFollowBtn();
                Post.SetVoteBtn(this, vote);


            },

            setFollowBtn: function(){
                Post.SetFollowBtn(this);

            },


            voteUp: function(ev){
                this.vote(1)
            },

            voteDown: function(ev){
                this.vote(-1)
            },

            vote: function(vote) {

                this.setBtn(vote);
                Post.Vote(this, vote);

            },

            voteSuccess: function(){

                this.setBtn();

            },

            browsePost: function(ev) {
                app.router.go("post", this.model.id);
            },

            initialize: function(el) {
                this.listenTo(this.model, 'change', this.render);
                this.model.on("change:my_vote_number", this.voteSuccess, this);
            },

            isFollowedActive: function () {
                return this.post['is_followed'] ? 'active' : '';
            },

            isFollowNum: function () {
                return this.post['is_followed'] ? 1 : 0;
            },

            voteUpNumConvert: function () {
                return this.post['my_vote_number'] == 1 ? 0 : 1;
            },

            voteDownNumConvert: function () {
                return this.post['my_vote_number'] == -1 ? 0 : -1;
            }
        });

        Post.Views.ItemEnd = Backbone.View.extend({
            template: 'post/itemend',

            tagName: 'div',

            events: {
                "click .top"       : "scrollTop"
            },

            initialize: function() {

            },

            scrollTop: function(){
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }

        });

        Post.Views.Loading = Backbone.View.extend({
            template: 'post/loading',

            tagName: 'div',

            className: 'loading',


            initialize: function() {

            }

        });



        Post.Views.ItemRendered = Backbone.View.extend({

            events: {
                "click .vote .up a"       : "voteUp",
                "click .vote .down a"       : "voteDown",
                "click .action .follow"       : "follow"
            },

            initialize: function() {
                this.model.on("change:follower_count", this.modelChanged, this);
            },

            modelChanged: function() {
                this.$(".action .follow .follow_count").html(this.model.get("follower_count"))
            },

            follow: function(evt){
                evt.preventDefault();

                var isFollowed = this.model.get("is_followed");
                var newFollow = true;

                if(isFollowed) {
                    newFollow = false;
                }

                Post.Follow(this, newFollow)
            },

            setBtn: function(vote){
                Post.SetVoteBtn(this, vote);
            },


            voteUp: function(evt){
                evt.preventDefault();
                this.vote(1)
            },

            voteDown: function(evt){
                evt.preventDefault();
                this.vote(-1)
            },

            vote: function(vote) {
                this.setBtn(vote);
                Post.Vote(this, vote);
            },

            voteSuccess: function(){
                console.log("votesuccess")
                this.setBtn();

            }

        });

        Post.Views.List = Backbone.View.extend({
            //template: 'post/list',

            //className: 'post-list',

            el: '.post-list',

            serialize: function() {
                return { current_forum: this.collection.forum};
            },

            initialize: function() {

                //hide pagination
                $( ".pagination").hide();

                var postsList = this;
                //collection fetching status
                this.options.postFetching = false;
                //current page loaded
                this.options.page = 1;
                //check if there are no more posts
                this.options.postEnd = false;

                //get posts
                this.$( ".post-item" ).each(function( index ) {

                    var points = parseInt($(this).find(".vote .display").html());
                    var postId = $(this).attr("data-id");
                    var myvote = parseInt($(this).attr("data-myvote"));
                    var follower = parseInt($(this).attr("data-follower"));
                    var isfollowed = ($(this).attr("data-isfollowed")=="True") ? true : false;

                    var post = new Post.Model()
                    post.set("entity_id", postId)
                    post.set("points", points)
                    post.set("my_vote_number", myvote)
                    post.set("is_followed", isfollowed)
                    post.set("follower_count", follower)

                    new Post.Views.ItemRendered({el: this, model: post});

                });


                //append endless scroll
                $(document).endlessScroll({
                    bottomPixels: 200,
                    fireDelay: 10,
                    fireOnce: true,
                    loader: "",
                    callback: function(i){
                        //check if post list is shown

                        if($.find('.post-list').length != 0)
                        {
                            if(postsList.options.postFetching == false && !postsList.options.postEnd){


                                if($.find('.loading').length == 0){
                                    postsList.insertView('.content', new Post.Views.Loading()).render();
                                    console.log("init loading")
                                }else{
                                    console.log("insert after")
                                    $('.loading').insertAfter($('.content'));
                                    $('.loading').show();
                                }

                                postsList.loadNewPosts();

                                postsList.options.postFetching = true;
                            }
                        }
                    }
                });
            },

            beforeRender: function() {

                this.collection.each(function(post) {
                    this.insertView('.content', new Post.Views.Item({
                        model: post,
                        selected: post.id
                    }));
                }, this);

            },

            afterRender: function() {
                this.$('.loading').hide();
            },

            loadNewPosts: function(ev) {

                this.options.page++;
                var viewList = this;

                var fetchCollection = new Post.Collection();

                //load new posts
                fetchCollection.fetch({update: true, remove:true,
                    data: {
                        page: this.options.page,
                        page_size: 10,
                        forum_id: _.isNumber(parseInt(this.options.selectedForum))  ? this.options.selectedForum : 0
                    },

                    success: function(loadedPosts){
                        loadedPosts.each(function(post) {
                            //append new posts

                            //viewList.collection.add(post);
                            viewList.insertView('.content', new Post.Views.Item({
                                model: post,
                                selected: post.id
                            })).render();
                        });
                        viewList.options.postFetching = false;
                        viewList.$('.loading').hide();
                        console.log("loading hide");
                        $(".post-list").find(".loading").hide()
                    },

                    error: function(error){
                        viewList.options.postEnd = true;
                        viewList.insertView('.content', new Post.Views.ItemEnd()).render();
                        $('.loading').hide();
                    }
                });

            }
        });

        Post.Views.NavDefault = Backbone.View.extend({
            template: 'post/contentnav/default',

            page: "default",

            events: {
                'click #addPost': 'addPost'
            },

            initialize: function () {


            },

            beforeRender: function() {

                this.insertView('#selectforum', new Forum.Views.List({ collection: this.options.forumCollection, selected: this.options.selectedForum}));
            },

            afterRender: function() {

            },

            addPost: function(){
                app.router.go("forum", this.getSelectedForum(), "add");
            },

            getSelectedForum: function(){
                var rootForum = this.options.forumCollection.where({level: 0})
                var rootForumId;

                if(!_.isNumber(this.options.selectedForum)){

                    for( var i in rootForum )
                    {
                        var currentNode = rootForum[i];
                        rootForumId = currentNode.get("entity_id")
                    }

                    return rootForumId
                }else{
                    return this.options.selectedForum
                }
            }


        });

        Post.Views.NavAddPost = Backbone.View.extend({
            template: 'post/contentnav/addpost'

        });

        Post.Views.NavEditPost = Backbone.View.extend({
            template: 'post/contentnav/editpost'

        });

        Post.Views.NavViewPost = Backbone.View.extend({

            el: '#contentmenu',

            events: {
                'click #edit': 'edit',
                'click #delete button': 'delete',
                'click #cancelDeletePost': 'cancel',
                'click #deletePost': 'confirmDelete',
                "click .status .follow": "follow",
                "click .vote .up a": "voteUp",
                "click .vote .down a": "voteDown"
            },

            initialize: function () {

                var dataAttr = this.$(".nav");

                var postId = $(dataAttr).attr("data-id");
                var myvote = parseInt($(dataAttr).attr("data-myvote"));
                var follower = parseInt($(dataAttr).attr("data-follower"));
                var isfollowed = ($(dataAttr).attr("data-isfollowed")=="True") ? true : false;

                var post = new Post.Model()
                post.set("entity_id", postId)
                post.set("my_vote_number", myvote)
                post.set("is_followed", isfollowed)
                post.set("follower_count", follower)

                this.model = post;

                this.model.on("change:follower_count", this.followChanged, this);

            },

            followChanged: function() {
                this.$(".status .follow .follow_count").html(this.model.get("follower_count"))
            },

            follow: function(evt){
                evt.preventDefault();

                var isFollowed = this.model.get("is_followed");
                var newFollow = true;

                if(isFollowed) {
                    newFollow = false;
                }

                Post.Follow(this, newFollow)
            },

            edit: function(){
                app.router.go("post", this.options.postId, "edit");
            },

            delete: function(evt){
                evt.preventDefault();
                this.$('#delete').hide();
                this.$('#confirmDelete').show();
            },

            cancel: function(){
                this.$('#delete').show();
                this.$('#confirmDelete').hide();
            },

            confirmDelete: function(){
                if(!$('#deletePost').hasClass('disabled'))
                {
                    this.$("#delete form").submit();
                }

                this.$('#deletePost').addClass("disabled").html('<i class="icon-spinner icon-spin"></i> Yes, delete it!');
                this.$('#deletePost').click(function(){})
            }

        });



        return Post;

    });



