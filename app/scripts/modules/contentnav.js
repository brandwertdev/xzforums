define([
    // Libararies.
    "backbone",

    // Application.
    "app",
    "modules/forum"
],

function(Backbone, app, Forum) {
    "use strict";

    var ContentNav = app.module();



    ContentNav.PostView = Backbone.View.extend({
        template: 'navigation/contentnav/postview',

        serialize: function() {

        },

        beforeRender: function() {


        },

        events: {

        },

        initialize: function() {
            this.render()
            console.log("asdf")
        }
    });

    ContentNav.Default = Backbone.View.extend({
        template: 'navigation/contentnav/default',

        serialize: function() {

        },

        beforeRender: function() {
            this.insertView('#selectforum', new Forum.Views.List({ collection: this.collection, selected: this.options.selectedForum}));
        },

        events: {

        },

        initialize: function() {
            this.render()
        }
    });


    return ContentNav;

});
