// This module is used to upload image to transloadit.

define([
    // Libararies.
    'jquery',
    'lodash',

    // Application.
    'app',
	'modules/modalPopup',
    // Plugin.
    "transloadit",
	"jcrop",
	"slider"
],

function ($, _, app, ModalPopup) {
    'use strict';

	var UploadImage = app.module();

	UploadImage.Model = Backbone.Model.extend({
		idAttribute: 'entity_id',

		url: function() {
			return app.getApiUrl('media/signature') + '?' + 'template_id=b371e66ed5704d38a95f1d3dae117739';
		},

		default: {
			params: '',
			signature: ''
		},

		parse: function (resp) {
			return resp.auth;
		},

		getParams: function () {
			return this.get('params');
		},

		getSignature: function () {
			return this.get('signature');
		}
	});

	var model = new UploadImage.Model();

	UploadImage.Views.ModalWindow = ModalPopup.extend({
		template: 'upload/uploadImage',

		className: 'modal-window upload-image',

		model: model,

		events: {
			'click input:button[value=upload]': 'upload',
			'click input:button[value=close]': 'close'
		},

		hasImage: false,

		initialize: function () {
			this.model.fetch();

			this.listenTo(this.model, 'change', this.render);
		},

		serialize: function () {
			return this.model.toJSON();
		},

		afterRender: function () {
			var self = this;

			// This function is defined in modalPopup class.
			self.initModalWindow();

			self.fileEl = self.$el.find('input:file');
			self.formEl = self.$el.find('form');
			self.previewEl = self.$el.find('.image-preview img');

			self.setupTransliadit();

			self.fileEl.on('change', self, self.fileChanged);
		},

		setSizeFields: function () {
			var widthEl = this.$el.find('input:hidden[name=width]'),
				heightEl = this.$el.find('input:hidden[name=height]');

			widthEl.val(this.previewEl.width());
			heightEl.val(this.previewEl.height());
		},

		setCoordsFields: function (c) {
			var x1El = this.$el.find('[name=x]'),
				y1El = this.$el.find('[name=y]'),
				x2El = this.$el.find('[name=x2]'),
				y2El = this.$el.find('[name=y2]');

			x1El.val(c.x);
			y1El.val(c.y);
			x2El.val(c.x2);
			y2El.val(c.y2);
		},

		setupTransliadit: function () {
			var self = this;

			self.formEl.transloadit({
				//wait: true,
				autoSubmit: false,
				fields: true,
				onSuccess: function (result) {
					alert('successfully');

					$('.user-details .avatar-holder img').attr('src', self.imageData);


					$('.user-details .avatar-holder img').css({
						position: 'absolute',
						width: self.previewEl.width(),
						height: self.previewEl.height(),
						top: '-' + self.$el.find('[name=y]').val() + 'px',
						left: '-' + self.$el.find('[name=x]').val() + 'px'
					});

					self.close();
				}
			});
		},

		fileChanged: function (event) {
			var self = event.data,
				fileDom = this,
				oFReader = new FileReader(),
				rFilter = /^(?:image\/.*)$/i,
				oFile;

			if (fileDom.files.length === 0)
				return;

			oFile = fileDom.files[0];

			if (!rFilter.test(oFile.type)) {
				alert("You must select a valid image file!");
				return;
			}

			if (self.jcrop_api)
				self.jcrop_api.destroy();

			oFReader.onload = function (oFREvent) {
				self.imageData = oFREvent.target.result;
				self.previewEl.attr('src', self.imageData);

				self.hasImage = true;

				self.setSizeFields();

				setTimeout(function () {
					// TODO: we can resize uploaded img on-line.
//						$('.image-preview-holder div[data-action=size-slider]').slider({
//							value: self.previewEl.width(),
//							max: self.previewEl.width(),
//							handle: 'round'
//						}).on('slide', function (e) {
//								//self.previewEl.width(e.value);
//								$('.jcrop-holder img').width(e.value);
//							});

					var c = {"x":0,"y":0,"x2":200,"y2":200};

					self.previewEl.Jcrop({
						bgFade: true,
						setSelect: [c.x,c.y,c.x2,c.y2],
						keySupport:false,
						maxSize: [200, 200],
						minSize: [100, 100],
						createHandles: ['nw','ne','se','sw'],
						onSelect: function(c) { self.setCoordsFields(c) },
						onChange: function(c) { self.setCoordsFields(c) }
					}, function () {
						self.jcrop_api = this;
					});
				}, 500);
			};

			oFReader.readAsDataURL(oFile);
		},

		upload: function () {
			if (!this.hasImage)
				alert('please select one image!');
			else {
				this.formEl.submit();
			}
		}
	});

    return UploadImage;
});
