define([
	// Libararies.
	"jquery",
	"lodash",
	"backbone",

	// Application.
	"app"
	], 

	function ($, _, Backbone, app) {
		'use strict';

		var Register = app.module();

		Register.Model = Backbone.Model.extend({
	        idAttribute: 'entity_id',

	        url: function() {
	            return app.getApiUrl('password_authentication/register');
	        },

	        default: {
	        	username: '',
	        	password: '',
	        	display_name: ''
	        },

	        setUsername: function (username) {
	        	this.set('username', username);
	        },

	        setPassword: function (password) {
	        	this.set('password', password);
	        },

	        setDisplayname: function (displayname) {
	        	this.set('display_name', displayname);
	        },

	        validate: function (attrs, options) {
	        	if(arguments.length == 0)
	        		return this.validate(this.attributes);

	        	var errors = {};

	        	// This validate part should be changed according to actural requirement.
	        	if(attrs.username.length == 0)
	        		errors.username = 'username should not be empty';

	        	if(attrs.password.length == 0)
	        		errors.password = 'password should not be empty';

	        	return _.isEmpty(errors) ? false : errors;
	        }
	    });

		Register.View = Backbone.View.extend({
			template: 'forms/register',

			className: 'form',

			map: {

			},

			initialize: function () {

				// Listen to model, and handle server error
				this.listenTo(this.model, {
					error: this.onRegisterServerError
				});
			},

			afterRender: function () {
				// create reference to this object for convenient later.
				_.extend(this, {
					usernameEl: this.$('#register-username'),
					passwordEl: this.$('#register-password'),
					displaynameEl: this.$('#register-displayname')
				});
			},

			events: {
				'click #register-submit': 'submitRegister',
				// Delegated events. clear error style when focus in.
				'focus #register-username': 'clearUsernameError',
				'focus #register-password': 'clearPasswordError'
			},

			getUsernameValue: function () {
				return this.usernameEl.children('input')[0].value;
			},

			getPasswordValue: function () {
				return this.passwordEl.children('input')[0].value;
			},

			getDisplaynameValue: function () {
				return this.displaynameEl.val();
			},

			setModel: function (attrs) {
				this.model.setUsername(this.getUsernameValue());
				this.model.setPassword(this.getPasswordValue());
				this.model.setDisplayname(this.getDisplaynameValue());
			},

			validate: function () {
				return this.model.validate();
			},

			submitRegister: function () {
				this.$('.server-error').remove();

				this.setModel();

				var errors = this.validate();
				
				if (!_.isEmpty(errors)) 
					this.setErrors(errors);
				else 		
					this.model.save(null, {success: this.onRegisterServerSuccess});
			},

			setErrors: function (errors) {
				if(_.has(errors, 'username'))
					this.setInputError(this.usernameEl, errors.username);

				if(_.has(errors, 'password'))
					this.setInputError(this.passwordEl, errors.password);
			},

			onRegisterServerSuccess: function () {
				app.router.go('');
			},

			onRegisterServerError: function (model, xhr, options) {
				var error = $.parseJSON(xhr.responseText);
				if(error && error.message)
					this.usernameEl.before('<div class="server-error">' + error.message + '</div>');
			},

			clearUsernameError: function () {
				this.clearInputError(this.usernameEl);
			},

			clearPasswordError: function () {
				this.clearInputError(this.passwordEl);
			},

			setInputError: function (field ,error) {
				if(!field.hasClass('error')) {
					field.addClass('error');
					field.prepend('<span class="help-inline">'+error+'</spam>');
				}
			},

			clearInputError: function (field) {
				if(field.hasClass('error')) {
					field.removeClass('error');
					field.children('.help-inline').remove();
				}
			}		
		});

		return Register;
});