define([
    // Libararies.
    "jquery",
    "backbone",

    // Application.
    "app",
    "modules/uploadImage"
],

function ($, Backbone, app, UploadImage) {
    'use strict';

    var Userprofile = app.module();

    Userprofile.Model = Backbone.Model.extend({
        idAttribute: 'entity_id',

        default: {
            entity_id: 0,
            display_name: ''
        },

        initialize: function () {
        },

        url: function() {
            return app.getApiUrl('user_profile');
        },

        parse: function (resp) {
            return resp.user;
        },

        getAvatar: function () {
            return this.get('avatar') ? this.get('avatar') : '../images/default_avatar.jpeg';
        },

        getDisplayname: function () {
            return this.get('display_name') ? this.get('display_name') : 'you don\'t set display name!';
        },

        getLastLogin: function () {
            return this.get('last_login');
        },

        getFollowings: function () {
            return this.get('following');
        },

        getId: function () {
            return this.id;
        }
    });

    Userprofile.Views.ContentNav = Backbone.View.extend({
        template: 'userprofile/contentnav',

        className: 'nav-collapse nav-collapse-right',

        events: {
            'click [data-action=toggle-menu]': 'toggleMenu',
	        'click [data-action=upload-image]': 'showUploadImageWindow'
        },

	    serialize: function () {

		    return {
			    params: '',
			    signatrue: ''
		    };
	    },

        toggleMenu: function (e) {
            var targetEl = $(e.currentTarget),
                dropdownEl = targetEl.next('.dropdown-list');

            if (dropdownEl.css('display') == 'none')
                dropdownEl.parents('.nav-collapse-right').height(460);
            else
                dropdownEl.parents('.nav-collapse-right').height(260);

            dropdownEl.slideToggle();
        },

	    showUploadImageWindow: function () {
			app.appview.showModalWindow(new UploadImage.Views.ModalWindow());
	    }
    });

    Userprofile.Views.User = Backbone.View.extend({
        template: 'userprofile/user-account',

        className: 'user-details',

        serialize: function () {
            return {
                avatar:this.model.getAvatar(),
                displayName: this.model.getDisplayname(),
                lastLogin: this.model.getLastLogin()
            };
        }
    });

    return Userprofile;
});