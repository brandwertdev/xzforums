define([
	// Libararies.
	"jquery",
	"lodash",
	"backbone",

	// Application.
	"app",
    "modules/userprofile"
	], 

	function ($, _, Backbone, app, Userprofile) {
		'use strict';

		var Login = app.module();

		Login.Model = Backbone.Model.extend({
	        idAttribute: 'entity_id',

	        url: function() {
	            return app.getApiUrl('password_authentication/login');
	        },

	        default: {
	        	username: '',
	        	password: ''
	        },

	        setUsername: function (username) {
	        	this.set('username', username);
	        },

	        setPassword: function (password) {
	        	this.set('password', password);
	        },

	        validate: function (attrs, options) {
	        	if(arguments.length == 0)
	        		return this.validate(this.attributes);

	        	var errors = {};

	        	// This validate part should be changed according to actural requirement.
	        	if(attrs.username.length == 0)
	        		errors.username = 'username should not be empty';

	        	if(attrs.password.length == 0)
	        		errors.password = 'password should not be empty';

	        	return _.isEmpty(errors) ? false : errors;
	        }
	    });

		Login.View = Backbone.View.extend({
			template: 'forms/login',

			className: 'form',

			map: {

			},

			initialize: function () {

				// Listen to model, and handle server error
				this.listenTo(this.model, {
					error: this.onLoginServerError
				});
			},

			afterRender: function () {
				// create reference to this object for convenient later.
				_.extend(this, {
					usernameEl: this.$('#login-username'),
					passwordEl: this.$('#login-password')
				});
			},

			events: {
				'click #login-submit': 'submitLogin',
				// Delegated events. clear error style when focus in.
				'focus #login-username': 'clearUsernameError',
				'focus #login-password': 'clearPasswordError'
			},

			getUsernameValue: function () {
				return this.usernameEl.children('input')[0].value;
			},

			getPasswordValue: function () {
				return this.passwordEl.children('input')[0].value;
			},

			setModel: function (attrs) {
				this.model.setUsername(this.getUsernameValue());
				this.model.setPassword(this.getPasswordValue());
			},

			validate: function () {
				return this.model.validate();
			},

			submitLogin: function () {
				this.$('.server-error').remove();

				this.setModel();

				var errors = this.validate();
				
				if (!_.isEmpty(errors)) 
					this.setErrors(errors);
				else
					this.model.save({
						success: this.onLoginServerSuccess,
						xhrFields: {
							withCredentials: true
						}
					});
			},

			setErrors: function (errors) {
				if(_.has(errors, 'username'))
					this.setInputError(this.usernameEl, errors.username);

				if(_.has(errors, 'password'))
					this.setInputError(this.passwordEl, errors.password);
			},

			onLoginServerSuccess: function () {
				app.userprofile.fetch({
					xhrFields: {
						withCredentials: true
					}
				});

				app.router.go('');

			},

			onLoginServerError: function (model, xhr, options) {
				var error = $.parseJSON(xhr.responseText);
				if(error && error.message)
					this.usernameEl.before('<div class="server-error">' + error.message + '</div>');
			},

			clearUsernameError: function () {
				this.clearInputError(this.usernameEl);
			},

			clearPasswordError: function () {
				this.clearInputError(this.passwordEl);
			},

			setInputError: function (field ,error) {
				if(!field.hasClass('error')) {
					field.addClass('error');
					field.prepend('<span class="help-inline">'+error+'</spam>');
				}
			},

			clearInputError: function (field) {
				if(field.hasClass('error')) {
					field.removeClass('error');
					field.children('.help-inline').remove();
				}
			}		
		});

		return Login;
});